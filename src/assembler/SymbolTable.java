package assembler;

import assembler.file.Sorter;

import java.util.Iterator;
import java.util.LinkedList;

public class SymbolTable {

    public static void checkSectionsOverlap( SymbolTable table )
    {
        LinkedList<SymbolTableElement> sections = new LinkedList<>();
        Iterator<SymbolTableElement> it = table.list.iterator();
        SymbolTableElement el, el1, el2;

        while( it.hasNext() )
        {
            el = it.next();
            if ( el.isSection )
                sections.add(el);
        };

        Iterator<SymbolTableElement> it1 = sections.iterator();
        Iterator<SymbolTableElement> it2;
        while( it1.hasNext() )
        {
            el1 = it1.next();
            it2 = sections.iterator();

            while( it2.hasNext() )
            {
                el2 = it2.next();
                if ( el1.name.equals(el2.name) )
                    continue;
                if ( (el1.getSectionAddress() + el1.size >= el2.getSectionAddress()) && el2.getSectionAddress() > el1.getSectionAddress() ||
                        (el2.getSectionAddress() + el2.size >= el1.getSectionAddress()) && el1.getSectionAddress() > el2.getSectionAddress() )
                    ErrorHandler.throwException(
                            "Section '" + el1.name + "' and section '" + el2.name + "' overlap. Please change start addresses",
                            null);
            };

        };
    }

    private RelocationTable relocTable;
    private SymbolTableElement section;
    private int locationCounter;
    private LinkedList<SymbolTableElement> list;

    public SymbolTable()
    {
        list = new LinkedList<>();
        relocTable = new RelocationTable(this);
        locationCounter = 0;
    }

    public boolean insertSymbol( String name )
    {
        if ( symbolExists(name) )
            return false;

        SymbolTableElement el = new SymbolTableElement();
        el.name = name;
        el.addr = locationCounter;
        el.section = section;
        list.add(el);
        return true;
    }

    public boolean symbolExists( String name )
    {
        SymbolTableElement el;
        Iterator<SymbolTableElement> it = list.iterator();
        while( it.hasNext() )
        {
            el = it.next();
            if ( el.name.equals(name) )
                return true;
        };
        return false;
    }

    public SymbolTableElement getSymbol( String name )
    {
        SymbolTableElement el;
        Iterator<SymbolTableElement> it = list.iterator();
        while( it.hasNext() )
        {
            el = it.next();
            if ( el.name.equals(name) )
                return el;
        };
        return null;
    }

    public boolean sectionExists( String name )
    {
        SymbolTableElement el;
        Iterator<SymbolTableElement> it = list.iterator();
        while( it.hasNext() )
        {
            el = it.next();
            if ( el.name.equals(name) && el.isSection )
                return true;
        };
        return false;
    }

    public void resetLocationCounter()
    {
        resetLocationCounter(null);
    }

    public void resetLocationCounter( String section )
    {
        locationCounter = 0;
        if ( section != null ) {
            this.section = getSymbol(section);
        };
    }

    public void incLocationCounter()
    {
        incLocationCounter(1);
    }

    public int getLocationCounter()
    {
        return locationCounter;
    }

    public int getSectionLocationCounter()
    {
        return section.getLocationCounter();
    }

    public void incLocationCounter(int inc)
    {
        locationCounter += inc;
    }

    public RelocationTable getRelocationTable()
    {
        return relocTable;
    }

    public boolean insertSection( String name )
    {
        if ( sectionExists(name) )
            return false;
        if ( section != null )
            section.size = locationCounter;
        SymbolTableElement el = new SymbolTableElement();
        el.name = name;
        el.isSection = true;
        list.add(el);
        section = el;
        locationCounter = 0;
        return true;
    }

    public SymbolTableElement getCurrentSection()
    {
        return section;
    }

    public void finishFirstPass()
    {
        if ( section != null )
        {
          section.size = locationCounter;
        };
        section = null;
        locationCounter = 0;
    }

    public String dump()
    {
        StringBuilder str = new StringBuilder();
        String value;
        int val;
        SymbolTableElement el;
        Iterator<SymbolTableElement> it = list.iterator();
        str.append("======================SYMBOL TABLE DUMP=========================\n");

        String space = "%3s";
        String format = "%5s" + space + "%-35s" + space +  "%12s" + space + "%-12s" + space + "%10s" + space + "%10s\n";
        str.append( String.format(format,"Rbr.", " ", "Name", " ", "Value(Dec)", " ", "Value(Hex)" , " ", "IsSection", " ", "Size(B)") );
        int i = 1;
        while( it.hasNext() )
        {
          el = it.next();
          val =  el.isSection ? el.getSectionAddress() : el.addr;
          value = String.format("0x%04X", val);
          str.append( String.format(format, ""+i, " ", (el.isSection ? "" : "      ") + el.name, " ", val,
                  " ",
                  value
                  , " ", el.isSection ? "Yes" : "-", " ", el.isSection ? el.size : "-") );
          i++;
        };

        return str.toString();
    }

    public void prepareForSecondPass()
    {
        SymbolTableElement ste;
        Iterator<SymbolTableElement> it = list.iterator();
        while( it.hasNext() ) {
            ste = it.next();
            ste.prepareForSecondPass();
        };
    }

    public void insertIntoSection(byte b)
    {
        if ( section != null )
        {
          section.insertByte(b);
        };
    }

    public String getAllSectionsContent()
    {
        StringBuilder s = new StringBuilder();
        SymbolTableElement ste;
        Iterator<SymbolTableElement> it = list.iterator();
        int i=0;
        while( it.hasNext() ) {
            ste = it.next();
            if ( ste.isSection ) {
                if ( i > 0 )
                    s.append("\n\n");
                s.append(ste.getSectionContent());
                i++;
            };
        };
        return s.toString();
    }

    public boolean isSymbolInTheCurrentSection( SymbolTableElement sym )
    {
        return sym.section == this.section;
    }

    public LinkedList<SymbolTableElement> getAllSections()
    {
        SymbolTableElement el;
        Iterator<SymbolTableElement> it = list.iterator();
        LinkedList<SymbolTableElement> sections =  new LinkedList<>();
        while( it.hasNext() )
        {
            el = it.next();
            if ( el.isSection )
                sections.add(el);
        };
        sections.sort(new Sorter());
        return sections;
    }
}
