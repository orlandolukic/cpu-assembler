package assembler.pass;

import assembler.ErrorHandler;
import assembler.Functions;
import assembler.SymbolTable;
import grammar.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.Iterator;
import java.util.LinkedList;

public class FirstPass extends asBaseListener {

    private boolean isRept;
    private boolean isReptGiven;
    private SymbolTable table;
    private boolean isBZ;

    public FirstPass(SymbolTable table)
    {
        this(table, false);
    }

    public FirstPass( SymbolTable table, boolean isRept )
    {
        this.table = table;
        this.isRept = isRept;
        this.isReptGiven = isRept;
    }

    private boolean ignore()
    {
        return isRept;
    }

    @Override
    public void exitI_DEC(asParser.I_DECContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_HALT(asParser.I_HALTContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_INC(asParser.I_INCContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_IRET(asParser.I_IRETContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_POP(asParser.I_POPContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_PUSH(asParser.I_PUSHContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_RET(asParser.I_RETContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_CLI(asParser.I_CLIContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_LDIMR(asParser.I_LDIMRContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_LDSP(asParser.I_LDSPContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_STI(asParser.I_STIContext ctx) {
        if (ignore())
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_STIMR(asParser.I_STIMRContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitI_STSP(asParser.I_STSPContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void enterIA_ADD(asParser.IA_ADDContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_AND(asParser.IA_ANDContext ctx) {
        if (ignore())
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_LD(asParser.IA_LDContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_OR(asParser.IA_ORContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_ST(asParser.IA_STContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_CMP(asParser.IA_CMPContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_MUL(asParser.IA_MULContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_NOT(asParser.IA_NOTContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_SUB(asParser.IA_SUBContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterIA_XOR(asParser.IA_XORContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void exitIB_BZ(asParser.IB_BZContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
        isBZ = true;
    }

    @Override
    public void exitIB_JMP(asParser.IB_JMPContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_CALL(asParser.IB_CALLContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_JEQ(asParser.IB_JEQContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_JGE(asParser.IB_JGEContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_JGT(asParser.IB_JGTContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_JLE(asParser.IB_JLEContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_JLT(asParser.IB_JLTContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    @Override
    public void exitIB_JNEQ(asParser.IB_JNEQContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter();
    }

    /**
     * Addressing!
     */

    @Override
    public void exitImmediateNumber(asParser.ImmediateNumberContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void exitImmediateLabel(asParser.ImmediateLabelContext ctx) {
        if (ignore())
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void exitRegisterIndirect(asParser.RegisterIndirectContext ctx) {
        if ( ignore() )
            return;

        if ( ctx.registerIndirectDisp() == null )
            return;

        table.incLocationCounter(2);
    }

    @Override
    public void exitMemoryIndirect(asParser.MemoryIndirectContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void exitMemoryDirect(asParser.MemoryDirectContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void exitMemoryDirectLabel(asParser.MemoryDirectLabelContext ctx) {
        if ( ignore() )
            return;
        if ( Functions.isRegister( ctx.labelName().getText() ) )
            return;
        table.incLocationCounter(isBZ ? 0 : 2);
    }



    // Directive handling

    private int reptDimension = 0;
    private int check = 0;
    private LinkedList<ParserRuleContext> reptContexts = new LinkedList<>();

    @Override
    public void exitLabel(asParser.LabelContext ctx) {
        if ( ignore() )
            return;
        if ( !table.insertSymbol( ctx.labelName().getText() ) )
            ErrorHandler.throwException("Symbol with name '" + ctx.labelName().getText() + "' is already defined", ctx);
    }

    @Override
    public void exitAdsNumber(asParser.AdsNumberContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(check);
    }

    @Override
    public void enterAdsLabel(asParser.AdsLabelContext ctx) {
        if ( ignore() )
            return;
        table.incLocationCounter(2);
    }

    @Override
    public void enterD_byte(asParser.D_byteContext ctx) {
        if ( ignore() )
            return;
        check = 1;
    }

    @Override
    public void enterD_word(asParser.D_wordContext ctx) {
        if ( ignore() )
            return;
        check = 2;
    }

    @Override
    public void enterD_int(asParser.D_intContext ctx) {
        if ( ignore() )
            return;
        check = 2;
    }

    @Override
    public void exitD_allocation(asParser.D_allocationContext ctx) {
        if ( ignore() )
            return;
        check = 0;
    }

    @Override
    public void exitD_asciz(asParser.D_ascizContext ctx) {
        if ( ignore() )
            return;
        String s = ctx.StringLiteral().getText();
        s = s.replaceAll("\"", "");
        s = s.replaceAll("\\\\n|\\\\r\\\\n", "\n");
        table.incLocationCounter(s.length() + 1);
    }

    @Override
    public void exitD_ascii(asParser.D_asciiContext ctx) {
        if ( ignore() )
            return;
        String s = ctx.StringLiteral().getText();
        s = s.replaceAll("\"", "");
        s = s.replaceAll("\\\\n|\\\\r\\\\n", "\n");
        table.incLocationCounter(s.length());
    }

    @Override
    public void exitD_skip(asParser.D_skipContext ctx) {
        if ( ignore() )
            return;
        String val = ctx.NumValue().getText();
        int value = Integer.parseInt(val);
        table.incLocationCounter(value);
    }

    @Override
    public void exitD_section(asParser.D_sectionContext ctx) {
        if ( ignore() )
            return;
        String sectionName = ctx.SectionName().getText();
        if ( table.symbolExists( sectionName ) )
            ErrorHandler.throwException("Section with name '" + sectionName + "' already exists", ctx);
        table.insertSection(sectionName);
    }

    @Override
    public void exitOptionLabelOnly(asParser.OptionLabelOnlyContext ctx) {
        if ( ignore() )
            return;
        if ( !table.insertSymbol(ctx.labelOnly().getText()) )
            ErrorHandler.throwException("Symbol '" + ctx.labelOnly().getText() + "' is already defined", ctx);
    }

    @Override
    public void enterD_rept(asParser.D_reptContext ctx) {
        if ( isReptGiven )
            return;
        isRept = true;
    }

    @Override
    public void exitD_rept(asParser.D_reptContext ctx) {
        isRept = true;
        reptDimension = Integer.parseInt( ctx.NumValue().getText() );
    }

    @Override
    public void exitD_endr(asParser.D_endrContext ctx) {
        isRept = false;
        ParseTreeWalker w = new ParseTreeWalker();
        ParserRuleContext c;
        FirstPass fp1 = new FirstPass(table);
        int i = 0;
        while( i < reptDimension ) {
            Iterator<ParserRuleContext> it = reptContexts.iterator();
            while (it.hasNext()) {
                c = it.next();
                w.walk(fp1, c);
            };
            i++;
        };
        reptDimension = 0;
        reptContexts.clear();
    }

    @Override
    public void enterOptionDirectiveNotation(asParser.OptionDirectiveNotationContext ctx) {
        if ( isRept )
            reptContexts.add(ctx);
    }

    @Override
    public void enterOptionInstructionNotation(asParser.OptionInstructionNotationContext ctx) {
        if ( isRept )
            reptContexts.add(ctx);
        isBZ = false;
    }

    @Override
    public void exitTranslationUnit(asParser.TranslationUnitContext ctx) {
        table.finishFirstPass();
    }
}
