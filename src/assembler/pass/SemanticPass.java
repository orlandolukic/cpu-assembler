package assembler.pass;

import assembler.ErrorHandler;
import assembler.Functions;
import assembler.Instructions;
import grammar.*;
import org.antlr.v4.runtime.ParserRuleContext;

public class SemanticPass extends asBaseListener {

    private boolean isRept;
    private byte instruction;
    private boolean onlyOneByte;
    private boolean onlyMemoryDirect;
    private boolean isBZ;

    public SemanticPass()
    {
        onlyOneByte = false;
    }

    @Override
    public void enterI_HALT(asParser.I_HALTContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.HALT;
    }

    @Override
    public void enterI_RET(asParser.I_RETContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.RET;
    }

    @Override
    public void enterI_IRET(asParser.I_IRETContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.IRET;
    }

    @Override
    public void enterI_PUSH(asParser.I_PUSHContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.PUSH;
    }

    @Override
    public void enterI_POP(asParser.I_POPContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.POP;
    }

    @Override
    public void enterI_DEC(asParser.I_DECContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.DEC;
    }

    @Override
    public void enterI_INC(asParser.I_INCContext ctx) {
        onlyOneByte = true;
        instruction = Instructions.INC;
    }

    @Override
    public void enterIA_ST(asParser.IA_STContext ctx) {
        instruction = Instructions.ST;
    }

    @Override
    public void enterIA_CMP(asParser.IA_CMPContext ctx) {
        instruction = Instructions.CMP;
    }

    @Override
    public void enterIA_OR(asParser.IA_ORContext ctx) {
        instruction = Instructions.OR;
    }

    @Override
    public void enterIA_LD(asParser.IA_LDContext ctx) {
        instruction = Instructions.LD;
    }

    @Override
    public void enterIA_AND(asParser.IA_ANDContext ctx) {
        instruction = Instructions.AND;
    }

    @Override
    public void enterIA_ADD(asParser.IA_ADDContext ctx) {
        instruction = Instructions.ADD;
    }

    @Override
    public void enterIA_XOR(asParser.IA_XORContext ctx) {
        instruction = Instructions.XOR;
    }

    @Override
    public void enterIA_SUB(asParser.IA_SUBContext ctx) {
        instruction = Instructions.SUB;
    }

    @Override
    public void enterIA_NOT(asParser.IA_NOTContext ctx) {
        instruction = Instructions.NOT;
    }

    @Override
    public void enterIA_MUL(asParser.IA_MULContext ctx) {
        instruction = Instructions.MUL;
    }

    @Override
    public void enterIB_JEQ(asParser.IB_JEQContext ctx) {
        instruction = Instructions.JEQ;
    }

    @Override
    public void enterIB_JGE(asParser.IB_JGEContext ctx) {
        instruction = Instructions.JGE;
    }

    @Override
    public void enterIB_JGT(asParser.IB_JGTContext ctx) {
        instruction = Instructions.JGT;
    }

    @Override
    public void enterIB_JLE(asParser.IB_JLEContext ctx) {
        instruction = Instructions.JLE;
    }

    @Override
    public void enterIB_JLT(asParser.IB_JLTContext ctx) {
        instruction = Instructions.JLE;
    }

    @Override
    public void enterIB_JNEQ(asParser.IB_JNEQContext ctx) {
        instruction = Instructions.JNEQ;
    }

    @Override
    public void enterIB_CALL(asParser.IB_CALLContext ctx) {
        instruction = Instructions.CALL;
        onlyMemoryDirect = true;
    }

    @Override
    public void enterIB_JMP(asParser.IB_JMPContext ctx) {
        instruction = Instructions.JMP;
        onlyMemoryDirect = true;
    }

    @Override
    public void enterIB_BZ(asParser.IB_BZContext ctx) {
        instruction = Instructions.BZ;
        isBZ = true;
    }

    @Override
    public void exitImmediateNumber(asParser.ImmediateNumberContext ctx) {
        if ( instruction == Instructions.ST )
            ErrorHandler.throwException("Cannot use immediate addressing with store(ST) instruction", ctx);

        String txt;
        asParser.NumberContext number = ctx.number();
        if ( number instanceof asParser.NumberDecContext) {
              txt = ((asParser.NumberDecContext) (number)).NumValue().getText();
              if (  Integer.parseInt(txt) > ((1 << 16)-1) )
                  ErrorHandler.throwException("Value " + txt + "d is greater then 0xFFFF", ctx);
        } else if ( number instanceof asParser.NumberHexContext) {
            txt = ((asParser.NumberHexContext) number).HexValue().getText();
            txt = txt.replaceAll("^0[xX]", "");
            if ( txt.length() > 4 )
                ErrorHandler.throwException("Value 0x" + txt + " is greater then 0xFFFF", ctx);
        };
    }

    @Override
    public void exitOptionInstructionNotation(asParser.OptionInstructionNotationContext ctx) {
        onlyOneByte = false;
        onlyMemoryDirect = false;
        isBZ = false;
    }

    @Override
    public void exitAddressing(asParser.AddressingContext ctx) {
        if ( onlyOneByte ) {
            ErrorHandler.print("Could not use addressing with addressless instruction (" +
                    Instructions.getInstructionName(instruction, true) + ")"
            , ctx);
        }

        if ( onlyMemoryDirect ) {
            if ( ctx.immediate() != null || ctx.memoryIndirect() != null || ctx.registerIndirect() != null ||
                ctx.memoryDirectLabel() != null && Functions.isRegister( ctx.memoryDirectLabel().labelName().getText() )
            )
                ErrorHandler.throwException("Only direct memory addressing is allowed for instruction " +
                        Instructions.getInstructionName(instruction, true), ctx);
        };
    }

    @Override
    public void exitMemoryDirectLabel(asParser.MemoryDirectLabelContext ctx) {
        String r = ctx.labelName().getText();
        if ( Functions.isRegister(r) )
        {
            asParser.MinusMinusContext mc = ctx.minusMinus();
            asParser.PlusPlusContext pc = ctx.plusPlus();
            if ( mc != null && pc != null )
                ErrorHandler.throwException("Cannot use pre-increment and post-decrement at the same time", ctx);
        } else
        {
            if ( ctx.minusMinus() != null && ctx.plusPlus() != null )
                ErrorHandler.throwException("Pre-increment and post-decrement is only alowed on registers", ctx);
        };
    }

    @Override
    public void exitMemoryDirect(asParser.MemoryDirectContext ctx) {
        if ( isBZ )
            ErrorHandler.throwException("Cannot use displacement as number for instruction " +
                    Instructions.getInstructionName(instruction, true), ctx);
        Functions.processAddressLength(ctx);        
    }

    @Override
    public void exitMemoryIndirect(asParser.MemoryIndirectContext ctx) {
        Functions.processAddressLength(ctx);
    }

    // Directive handling.

    private int check = 0;

    @Override
    public void exitRegisterIndirect(asParser.RegisterIndirectContext ctx) {
        String val = ctx.LabelLiteral().getText();
        if ( val.matches("^[rR]\\d+") )
        {
            val = val.replaceAll("^[rR]", "");
            int regNo = Integer.parseInt(val);
            if ( regNo > 31 )
                ErrorHandler.throwException("Processor only has 32 general purpose registers (r0-r31)", ctx);

            if ( ctx.registerIndirectDisp() != null ) {
                Functions.processNumberLength(ctx.registerIndirectDisp().number());
            };
        } else
        {
            boolean add = ctx.registerIndirectDisp() != null;
            ErrorHandler.throwException("Addressing should be register indirect " + (add ? "with 16-bit displacement" : "") + " (r0-r31)", ctx);
        };
    }

    @Override
    public void exitAdsNumber(asParser.AdsNumberContext ctx) {
        asParser.NumberContext nc = ctx.number();
        String val;
        if ( nc instanceof asParser.NumberHexContext ) {
            val = ((asParser.NumberHexContext) nc).HexValue().getText();
            val = val.replaceAll("^0[xX]", "");
            if ( check == 1 ) {
                if (val.length() > 2)
                    ErrorHandler.throwException("Value 0x" + val + " is greater than 255(0xFF)", ctx);
            } else if ( check == 2 ) {
                if ( val.length() > 4 )
                    ErrorHandler.throwException("Value 0x" + val + " is greater than 2^16-1(0xFFFF)", ctx);
            }
        } else if ( nc instanceof asParser.NumberDecContext ) {
            val = ((asParser.NumberDecContext) nc).getText();
            int value;
            value = Integer.parseInt(val);
            if ( check == 1 ) {
                if (value > 255)
                    ErrorHandler.throwException("Value " + val + "d is greater than 255(0xFF)", ctx);
            } else if ( check == 2 ) {
                if ( (value & ~0xFFFF) > 0 )
                    ErrorHandler.throwException("Value " + val + "d is greater than 2^16-1(0xFFFF)", ctx);
            }
        } else if ( nc instanceof  asParser.NumberBinaryContext ) {
            val = ((asParser.NumberBinaryContext) nc).getText();
            int value = Functions.getValueFromImmediateBinary(val);
            if ( check == 1 ) {
                if (value > 255)
                    ErrorHandler.throwException("Value " + val + "d is greater than 255(0xFF)", ctx);
            } else if ( check == 2 ) {
                if ( (value & ~0xFFFF) > 0 )
                    ErrorHandler.throwException("Value " + val + "d is greater than 2^16-1(0xFFFF)", ctx);
            }
        };
    }

    @Override
    public void enterD_byte(asParser.D_byteContext ctx) {
        check = 1;
    }

    @Override
    public void enterD_word(asParser.D_wordContext ctx) {
        check = 2;
    }

    @Override
    public void enterD_int(asParser.D_intContext ctx) {
        check = 2;
    }

    @Override
    public void exitD_allocation(asParser.D_allocationContext ctx) {
        check = 0;
    }

    @Override
    public void exitD_rept(asParser.D_reptContext ctx) {
        isRept = true;
    }

    @Override
    public void exitD_endr(asParser.D_endrContext ctx) {
        if ( !isRept )
            ErrorHandler.throwException("Directive '.rept-.endr' is never opened", ctx);
        isRept = false;
    }

    @Override
    public void exitLabel(asParser.LabelContext ctx) {
        if ( isRept )
            ErrorHandler.throwException("Label definition '" + ctx.labelName().getText() + "' should not be present in repeating sequnce", ctx);
    }

    @Override
    public void exitTranslationUnit(asParser.TranslationUnitContext ctx) {
        if ( isRept )
            ErrorHandler.throwException("Directive '.rept' needs to be closed. Please provide '.endr' in your code", ctx);
    }
}
