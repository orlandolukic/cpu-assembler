package assembler.pass;

import assembler.*;
import grammar.*;

public class SecondPass extends asBaseListener {

    private ReptDirective rept;
    private SymbolTable table;
    private boolean noAddressing;
    private boolean isBZ;

    public SecondPass( SymbolTable table )
    {
        this.table = table;
        noAddressing = false;
        isBZ = false;
    }

    @Override
    public void enterTranslationUnit(asParser.TranslationUnitContext ctx) {
        noAddressing = false;
    }

    private boolean ignore()
    {
        return rept != null ? rept.ignore() : false;
    }

    @Override
    public void exitI_RET(asParser.I_RETContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.RET );
    }

    @Override
    public void exitI_PUSH(asParser.I_PUSHContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.PUSH );
    }

    @Override
    public void exitI_POP(asParser.I_POPContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.POP );
    }

    @Override
    public void exitI_IRET(asParser.I_IRETContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.IRET );
    }

    @Override
    public void exitI_INC(asParser.I_INCContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.INC );
    }

    @Override
    public void exitI_DEC(asParser.I_DECContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.DEC );
    }

    @Override
    public void exitI_HALT(asParser.I_HALTContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.HALT );
    }

    @Override
    public void exitI_STSP(asParser.I_STSPContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.STSP );
    }

    @Override
    public void exitI_STIMR(asParser.I_STIMRContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.STIMR );
    }

    @Override
    public void exitI_STI(asParser.I_STIContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.STI );
    }

    @Override
    public void exitI_LDSP(asParser.I_LDSPContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.LDSP );
    }

    @Override
    public void exitI_LDIMR(asParser.I_LDIMRContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.LDIMR );
    }

    @Override
    public void exitI_CLI(asParser.I_CLIContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.CLI );
    }

    @Override
    public void exitIA_ADD(asParser.IA_ADDContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.ADD );
    }

    @Override
    public void exitIA_AND(asParser.IA_ANDContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.AND );
    }

    @Override
    public void exitIA_LD(asParser.IA_LDContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.LD );
    }

    @Override
    public void exitIA_OR(asParser.IA_ORContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.OR );
    }

    @Override
    public void exitIA_ST(asParser.IA_STContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.ST );
    }

    @Override
    public void exitIA_CMP(asParser.IA_CMPContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.CMP );
    }

    @Override
    public void exitIA_MUL(asParser.IA_MULContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.MUL );
    }

    @Override
    public void exitIA_NOT(asParser.IA_NOTContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.NOT );
    }

    @Override
    public void exitIA_SUB(asParser.IA_SUBContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.SUB );
    }

    @Override
    public void exitIA_XOR(asParser.IA_XORContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.XOR );
    }

    @Override
    public void exitIB_BZ(asParser.IB_BZContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.BZ );
        isBZ = true;
    }

    @Override
    public void exitIB_JMP(asParser.IB_JMPContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JMP );
        noAddressing = true;
    }

    @Override
    public void exitIB_CALL(asParser.IB_CALLContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.CALL );
        noAddressing = true;
    }

    @Override
    public void exitIB_JNEQ(asParser.IB_JNEQContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JNEQ );
        noAddressing = true;
    }

    @Override
    public void exitIB_JLT(asParser.IB_JLTContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JLT );
        noAddressing = true;
    }

    @Override
    public void exitIB_JLE(asParser.IB_JLEContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JLE );
        noAddressing = true;
    }

    @Override
    public void exitIB_JGT(asParser.IB_JGTContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JGT );
        noAddressing = true;
    }

    @Override
    public void exitIB_JGE(asParser.IB_JGEContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JGE );
        noAddressing = true;
    }

    @Override
    public void exitIB_JEQ(asParser.IB_JEQContext ctx) {
        if ( ignore() )
            return;
        table.insertIntoSection( Instructions.JEQ );
        noAddressing = true;
    }

    @Override
    public void exitD_rept(asParser.D_reptContext ctx) {
        rept = new ReptDirective( Functions.getValueFromImmediateDecimal(ctx.NumValue().getText()), table);
        rept.start();
        first = true;
    }

    @Override
    public void exitD_endr(asParser.D_endrContext ctx) {
        rept.finish();
    }

    @Override
    public void exitD_asciz(asParser.D_ascizContext ctx) {
        if ( ignore() )
            return;
        String s = ctx.StringLiteral().getText();
        s = s.replaceAll("\"", "");
        s = s.replaceAll("\\\\n|\\\\r\\\\n", "\n");
        for (int i=0; i<s.length(); i++)
            table.getCurrentSection().insertByte((byte) s.charAt(i));
        table.getCurrentSection().insertByte((byte) 0);
    }

    @Override
    public void exitD_ascii(asParser.D_asciiContext ctx) {
        if ( ignore() )
            return;
        String s = ctx.StringLiteral().getText();
        s = s.replaceAll("\"", "");
        s = s.replaceAll("\\\\n|\\\\r\\\\n", "\n");
        for (int i=0; i<s.length(); i++)
            table.getCurrentSection().insertByte((byte) s.charAt(i));
    }

    // OptionEach start

    private boolean first = true;

    @Override
    public void exitOptionDirectiveNotation(asParser.OptionDirectiveNotationContext ctx) {
        if ( rept != null && rept.ignore() && !first )
            rept.insertLine((asParser.OptionsEachContext) ctx);

        first = false;
    }

    // OptionEach end

    @Override
    public void exitLabelName(asParser.LabelNameContext ctx) {
        if ( ignore() )
            return;
        String label = ctx.getText();

        // Check if label is actual register
        if ( Functions.isRegister(label) )
            return;

        if ( !table.symbolExists(label) )
            ErrorHandler.throwException("Symbol '" + label + "' is not defined", ctx);
    }

    @Override
    public void exitOptionInstructionNotation(asParser.OptionInstructionNotationContext ctx) {
        noAddressing = false;
        isBZ = false;
    }

    @Override
    public void enterAddressing(asParser.AddressingContext ctx) {
        if ( ignore() )
            return;
        byte b1 = 0, b2 = 0, b3 = 0;
        if ( ctx.immediate() != null )
        {
            String v = null;
            int val = 0;
            boolean isLabelContext = false;
            SymbolTableElement lcel = null;
            asParser.ImmediateContext ic = ctx.immediate();
            if ( ic instanceof asParser.ImmediateNumberContext)
            {
                asParser.NumberContext nc = ((asParser.ImmediateNumberContext) ic).number();
                if (nc instanceof asParser.NumberHexContext) {
                    v = ((asParser.NumberHexContext) nc).HexValue().getText();
                    val = Functions.getValueFromImmediateHexadecimal(v);
                } else if (nc instanceof asParser.NumberDecContext) {
                    v = ((asParser.NumberDecContext) nc).NumValue().getText();
                    val = Functions.getValueFromImmediateDecimal(v);
                } else if ( nc instanceof asParser.NumberBinaryContext ) {
                    v = ((asParser.NumberBinaryContext) nc).BinValue().getText();
                    val = Functions.getValueFromImmediateBinary(v);
                };
            } else if ( ic instanceof asParser.ImmediateLabelContext )
            {
                String str = ((asParser.ImmediateLabelContext) ic).LabelLiteral().getText();
                if ( !table.symbolExists(str) )
                    ErrorHandler.throwException("Symbol '" + str + "' is not defined", ic);
                lcel = table.getSymbol(str);
                val = lcel.addr;
                isLabelContext = true;
            };

            b1 = (byte) (Instructions.IMMED << 5);
            b2 = Functions.getLowerByte((short) val);
            b3 = Functions.getHigherByte((short) val);

            table.insertIntoSection(b1);
            if ( isLabelContext )
            {
                table.getRelocationTable().addRelocationEntry(table.getSectionLocationCounter(), table.getCurrentSection(), lcel, RelocationEntry.R_PC_abs);
            };
            table.insertIntoSection(b2);
            table.insertIntoSection(b3);

        } else if ( ctx.registerIndirect() != null )
        {
            String reg = ctx.registerIndirect().LabelLiteral().getText();
            byte addrcode = ctx.registerIndirect().registerIndirectDisp() != null ? Instructions.REGINDPOM : Instructions.REGIND;
            table.insertIntoSection((byte) ((addrcode << 5) | Functions.getRegisterNumber(reg) ));
            if ( addrcode == Instructions.REGINDPOM )
            {
                int value = Functions.getValueFromNumber( ctx.registerIndirect().registerIndirectDisp().number() );
                if ( ctx.registerIndirect().registerIndirectDisp().minus() != null )
                    value = -value;
                table.insertIntoSection( Functions.getLowerByte((short)value) );
                table.insertIntoSection( Functions.getHigherByte((short)value) );
            };
        } else if ( ctx.memoryDirectLabel() != null )
        {
            String s = ctx.memoryDirectLabel().labelName().getText();
            SymbolTableElement el = table.getSymbol(s);
            if ( isBZ )
            {
                if (el == null)
                    ErrorHandler.throwException("Symbol '" + s + "' is not defined", ctx);
                System.out.println( Functions.getByteAsHexString((byte) table.getSectionLocationCounter()) );
                table.insertIntoSection((byte) ( el.addr - table.getSectionLocationCounter() - 1 ));
            } else {
                if (Functions.isRegister(s)) {
                    byte b = Instructions.REGDIR;
                    if ( ctx.memoryDirectLabel().plusPlus() != null )
                        b = Instructions.PREINCR;
                    else if ( ctx.memoryDirectLabel().minusMinus() != null )
                        b = Instructions.POSTDECR;
                    table.insertIntoSection((byte) ((b << 5) | Functions.getRegisterNumber(s)));
                } else {
                    if (!noAddressing)
                        table.insertIntoSection((byte) (Instructions.MEMDIR << 5));
                    int locCntRelocation = table.getSectionLocationCounter();
                    if (el == null)
                        ErrorHandler.throwException("Symbol '" + s + "' is not defined", ctx);
                    table.insertIntoSection(Functions.getLowerByte((short) el.addr));
                    table.insertIntoSection(Functions.getHigherByte((short) el.addr));

                    // Add relocation entry here!
                    int relocType = 0;
                    relocType = RelocationEntry.R_section;
                    SymbolTableElement sec = table.getCurrentSection();

                    table.getRelocationTable().addRelocationEntry(locCntRelocation, sec, el, relocType);
                }
                ;
            };
        } else if ( ctx.memoryDirect() != null )
        {
            String h = ctx.memoryDirect().HexValue().getText();
            int value = Functions.getValueFromImmediateHexadecimal(h);
            if ( !noAddressing )
                table.insertIntoSection( (byte) (Instructions.MEMDIR << 5) );
            table.insertIntoSection( Functions.getLowerByte( (short) value) );
            table.insertIntoSection( Functions.getHigherByte( (short) value) );
        } else if ( ctx.memoryIndirect() != null )
        {
            String h = ctx.memoryIndirect().HexValue().getText();
            int value = Functions.getValueFromImmediateHexadecimal(h);
            table.insertIntoSection( (byte) (Instructions.MEMIND << 5) );
            table.insertIntoSection( Functions.getLowerByte( (short) value) );
            table.insertIntoSection( Functions.getHigherByte( (short) value) );
        };
    }

    // Directives

    private int toGenerateByteNo = 0;

    @Override
    public void exitD_section(asParser.D_sectionContext ctx) {
        if ( ignore() )
            return;
        table.resetLocationCounter( ctx.SectionName().getText() );
    }

    @Override
    public void exitD_skip(asParser.D_skipContext ctx) {
        if ( ignore() )
            return;
        String val = ctx.NumValue().getText();
        int value = Integer.parseInt(val);
        for (int i=0; i<value; i++)
            table.insertIntoSection((byte) 0);
    }

    @Override
    public void enterD_int(asParser.D_intContext ctx) {
        if ( ignore() )
            return;
        toGenerateByteNo = 2;
    }

    @Override
    public void enterD_byte(asParser.D_byteContext ctx) {
        if ( ignore() )
            return;
        toGenerateByteNo = 1;
    }

    @Override
    public void enterD_word(asParser.D_wordContext ctx) {
        if ( ignore() )
            return;
        toGenerateByteNo = 2;
    }

    @Override
    public void exitAdsNumber(asParser.AdsNumberContext ctx) {
        if ( ignore() )
            return;
        int val = 0;
        val = Functions.getValueFromNumber( ctx.number() );

        if ( toGenerateByteNo == 2 )
        {
            table.insertIntoSection( Functions.getLowerByte( (short) val) );
            table.insertIntoSection( Functions.getHigherByte( (short) val ) );
        } else if ( toGenerateByteNo == 1 )
        {
            table.insertIntoSection( Functions.getLowerByte( (short) val) );
        };
    }

    @Override
    public void exitAdsLabel(asParser.AdsLabelContext ctx) {
        if ( ignore() )
            return;
        int val = 0;
        String sym = ctx.labelName().getText();
        SymbolTableElement e = table.getSymbol(sym);

        if ( toGenerateByteNo == 2 )
        {
            int lcnt = table.getSectionLocationCounter();

            // Add relocation entry.
            int relocType = 0;
            SymbolTableElement section;
            if ( table.isSymbolInTheCurrentSection(e) ) {
                val = e.addr;
                relocType = RelocationEntry.R_section;
                section = e.section;
            } else {
                relocType = RelocationEntry.R_PC_abs;
                section = table.getCurrentSection();
            };
            table.getRelocationTable().addRelocationEntry(lcnt, section, e, relocType);

            table.insertIntoSection( Functions.getLowerByte( (short) val) );
            table.insertIntoSection( Functions.getHigherByte( (short) val ) );
        };
    }
}
