package assembler;

import grammar.*;

public class Functions {

    public static void processAddressLength( asParser.MemoryDirectContext ctx )
    {
        String val = ctx.HexValue().getText();
        val = val.replaceAll("^0[xX]", "");
        String truncatedData = "";
        int truncatedDataVal;
        int addrLines = Integer.parseInt(EntryPoint.args[EntryPoint.E_MEMORY_ADDR_LINES]);
        if ( val.length()*4 > addrLines ) {
            truncatedDataVal = Integer.parseInt( val, 16 );
            truncatedDataVal &= ((1<<addrLines)-1);
            truncatedData = String.valueOf(Integer.toString(truncatedDataVal, 16));
            ErrorHandler.raiseWarning("Address '0x" + val + "' truncated to 0x" + truncatedData + " [addr_lines = " + addrLines + "]", ctx);
        };
    }

    public static void processAddressLength( asParser.MemoryIndirectContext ctx )
    {
        String val = ctx.HexValue().getText();
        val = val.replaceAll("^0[xX]", "");
        String truncatedData = "";
        int truncatedDataVal;
        int addrLines = Integer.parseInt(EntryPoint.args[EntryPoint.E_MEMORY_ADDR_LINES]);
        if ( val.length()*4 > addrLines ) {
            truncatedDataVal = Integer.parseInt( val, 16 );
            truncatedDataVal &= ((1<<addrLines)-1);
            truncatedData = String.valueOf(Integer.toString(truncatedDataVal, 16));
            ErrorHandler.raiseWarning("Address '0x" + val + "' truncated to 0x" + truncatedData + " [addr_lines = " + addrLines + "]", ctx);
        };
    }

    public static void processNumberLength( asParser.NumberContext ctx )
    {
        if ( ctx instanceof asParser.NumberBinaryContext ) {
            String val = ((asParser.NumberBinaryContext) ctx).BinValue().getText();
            val = val.replaceAll("^0[bB]", "");
            val = val.replaceAll("_", "");
            val = val.replaceAll("^[0]+", "");
            if ( val.length() > 16 )
                ErrorHandler.throwException("Value " + val + " is greater than 0xFFFF", ctx);
        } else if ( ctx instanceof  asParser.NumberDecContext ) {
            String s = ((asParser.NumberDecContext) ctx).NumValue().getText();
            try {
                int t = Integer.parseInt( s );
                if ( (t & ~0xFFFF) != 0 )
                    throw new Exception();
            } catch ( Exception e ) {
                ErrorHandler.throwException( "Number " + s + " is greater than 0xFFFF", ctx );
            }
        } else if ( ctx instanceof  asParser.NumberHexContext ) {
            String str = ((asParser.NumberHexContext) ctx).HexValue().getText().replaceAll("^0[xX]", "");
            str = str.replaceAll("^[0]*", "");
            if ( str.length() > 4 )
                ErrorHandler.throwException( "Number 0x" + str + " is greater than 0xFFFF", ctx );
        };
    }

    public static String getByteAsHexString( byte b )
    {
        return String.format("%02X", b);
    }

    public static boolean isRegister( String line )
    {
        boolean b = false;
        if ( line.matches("^[rR]\\d+") )
        {
            line = line.replaceAll("^[rR]","");
            int regNo = Integer.parseInt(line);
            if ( regNo <= 31 )
                b = true;
        };
        return b;
    }

    public static int getRegisterNumber( String line )
    {
        if ( line.matches("^[rR]\\d+") )
        {
            line = line.replaceAll("^[rR]","");
            int regNo = Integer.parseInt(line);
            return regNo;
        };
        return 0;
    }

    public static int getValueFromImmediateDecimal(String str)
    {
        return Integer.parseInt(str);
    }

    public static int getValueFromImmediateHexadecimal(String str)
    {
        str = str.replaceAll("^0[xX]", "");
        return Integer.parseInt(str, 16);
    }

    public static int getValueFromImmediateBinary(String str)
    {
        str = str.replaceAll("^0[bB]", "");
        str = str.replaceAll("_", "");
        str = str.replaceAll("^[0]+", "");
        return Integer.parseInt(str, 2);
    }



    public static byte getHigherByte(short value)
    {
        return (byte) ((value >> 8) & 0xFF);
    }

    public static byte getLowerByte(short value)
    {
        return (byte) (value  & 0xFF);
    }

    public static int getValueFromNumber(asParser.NumberContext number)
    {
        int v = 0;
        if ( number instanceof asParser.NumberDecContext ) {
            v = getValueFromImmediateDecimal(((asParser.NumberDecContext) number).NumValue().getText());
        } else if ( number instanceof asParser.NumberHexContext )
            v = getValueFromImmediateHexadecimal( ((asParser.NumberHexContext) number).HexValue().getText() );
        else if ( number instanceof asParser.NumberBinaryContext )
            v = getValueFromImmediateBinary( ((asParser.NumberBinaryContext) number).BinValue().getText() );
        return v;
    }
}
