package assembler;

@FunctionalInterface
public interface FI_BufferContent {

    void each(byte b, int i);
}
