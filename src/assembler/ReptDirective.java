package assembler;

import assembler.pass.SecondPass;
import grammar.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.Iterator;
import java.util.LinkedList;

public class ReptDirective {

    private SymbolTable table;
    private int n;
    private LinkedList<asParser.OptionsEachContext> list;
    private boolean ignore;

    public ReptDirective(int n, SymbolTable table)
    {
        list = new LinkedList<>();
        this.n = n;
        this.table = table;
    }

    public void start()
    {
        ignore = true;
    }

    public void insertLine( asParser.OptionsEachContext lnctx )
    {
        list.add(lnctx);
    }

    public boolean ignore()
    {
        return ignore;
    }

    public void finish()
    {
        ignore = false;
        asParser.OptionsEachContext c;
        Iterator<asParser.OptionsEachContext> it;
        SecondPass s = new SecondPass(table);
        ParseTreeWalker walker = new ParseTreeWalker();

        for (int i=0; i<n; i++)
        {
            it = list.iterator();
            while (it.hasNext())
            {
                c = it.next();
                walker.walk(s, c);
            };
        };
    }
}
