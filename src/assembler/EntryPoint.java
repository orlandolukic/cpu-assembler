package assembler;

import assembler.file.FileGeneration;
import assembler.pass.FirstPass;
import assembler.pass.SecondPass;
import assembler.pass.SemanticPass;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import grammar.*;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class EntryPoint {

    public static final boolean PRINT_STACK_TRACE = true;
    public static final int N = 6;
    public static final int E_OUTPUT = 4,
                            E_DATA_START_ADDRESS = 1,
                            E_TEXT_START_ADDRESS = 2,
                            E_MEMORY_ADDR_LINES = 3,
                            E_INPUT = 0,
                            E_DUMP = 5;

    private static boolean critical[];
    private static boolean entered[];
    public static String args[];
    private static String errorMessages[];
    private static boolean error;

    public static void main(String[] varg) {
        String a;
        entered = new boolean[N];
        args = new String[N];
        critical = new boolean[N];
        errorMessages = new String[N];

        // Set critical options & error messages.
        critical[E_OUTPUT] = true;
        errorMessages[E_OUTPUT] = "Molimo unesite ime izlaznog fajla u formatu: -o <OutputFile>";
        critical[E_INPUT] = true;
        errorMessages[E_INPUT] = "Molimo unesite ime ulaznog fajla u formatu: <InputFile>";
        critical[E_MEMORY_ADDR_LINES] = true;
        errorMessages[E_MEMORY_ADDR_LINES] = "Molimo unesite broj bitova za adresu memorije u formatu: -maddr=<bits>";

        boolean loadOutput = false;
        for (int i = 0; i < varg.length; i++) {
            a = varg[i];
            if (a.equals("-o") && i + 1 < varg.length) {
                loadOutput = true;
            } else if (a.startsWith("-sdata=")) {
                entered[E_DATA_START_ADDRESS] = true;
                args[E_DATA_START_ADDRESS] = a.replaceAll("^-sdata=", "");
            } else if (a.startsWith("-stext=")) {
                entered[E_TEXT_START_ADDRESS] = true;
                args[E_TEXT_START_ADDRESS] = a.replaceAll("^-stext=", "");
            } else if (a.startsWith("-maddr=")) {
                entered[E_MEMORY_ADDR_LINES] = true;
                args[E_MEMORY_ADDR_LINES] = a.replaceAll("^-maddr=", "");
            } else if ( a.equals("--dump") || a.equals("-d") ) {
                entered[E_DUMP] = true;
            } else {
                entered[ loadOutput ? E_OUTPUT : E_INPUT] = true;
                args[ loadOutput ? E_OUTPUT : E_INPUT ] = a;

                if ( loadOutput )
                    loadOutput = false;
            };
        };

        // Check critical options.
        for (int i = 0; i < N; i++)
            if (critical[i] && !entered[i]) {
                System.out.println("Koriscenje programa:");
                System.out.println("as <Options> <InputFile>");
                System.out.println(errorMessages[i]);
                return;
            }
        ;

        ParseTree tree = null;
        asParser parser = null;
        try {

            CharStream cs = fromFileName(args[E_INPUT]);
            asLexer lexer = new asLexer(cs);
            CommonTokenStream token = new CommonTokenStream(lexer);
            parser = new asParser(token);
            tree = parser.translationUnit();

        } catch (Exception e) {
            throw new RuntimeException("File '" + args[E_INPUT] + "' does not exist!");
        }

        SymbolTable table = new SymbolTable();

        try {
            // Check if there are syntax errors.
            if ( parser.getNumberOfSyntaxErrors() > 0 )
                throw new Exception();

            // Create walker for grammar traverse.
            ParseTreeWalker walker = new ParseTreeWalker();

            // Run semantic pass.
            SemanticPass sp = new SemanticPass();
            walker.walk(sp, tree);

            // First pass.
            FirstPass fp = new FirstPass(table);
            walker.walk(fp, tree);

            // Move section.
            if ( table.sectionExists(".text") )
            {
                if ( entered[E_TEXT_START_ADDRESS] )
                    table.getSymbol(".text").setSectionAddress(Functions.getValueFromImmediateHexadecimal(args[E_TEXT_START_ADDRESS]));
                else
                    table.getSymbol(".text").setSectionAddress(0x200);
            };
            if ( table.sectionExists(".data") )
            {
                if (entered[E_DATA_START_ADDRESS])
                    table.getSymbol(".data").setSectionAddress(Functions.getValueFromImmediateHexadecimal(args[E_DATA_START_ADDRESS]));
                else
                    table.getSymbol(".data").setSectionAddress(0x600);
            };

            // Check sections overlap.
            SymbolTable.checkSectionsOverlap(table);

            // Prepare table for second pass.
            table.prepareForSecondPass();

            // Second pass.
            SecondPass ndp = new SecondPass(table);
            walker.walk(ndp, tree);

            // Perform relocation.
            table.getRelocationTable().relocation();

            // Make logisim file.
            FileGeneration fg = new FileGeneration(table, args[E_OUTPUT]);
            fg.generate();

        } catch (RuntimeException e) {
            if ( PRINT_STACK_TRACE )
                e.printStackTrace();
            else
                System.out.println( e.getMessage() );
            error = true;
        } catch (Exception e) {
            error = true;
        };


        // Dump symbol table.
        if ( entered[E_DUMP] && !error ) {
            System.out.println(table.dump());
            System.out.println(table.getAllSectionsContent());
        };
    }
}
