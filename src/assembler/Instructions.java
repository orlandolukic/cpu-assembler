package assembler;

public class Instructions {

    // Instruction codes.
    public static final byte PUSH = (byte) 0x80,
                             POP = (byte) 0x81,
                             RET = (byte) 0x82,
                             IRET = (byte) 0x83,
                             HALT = (byte) 0x84,
                             INC = (byte) 0x85,
                             DEC = (byte) 0x86,
                             CLI = (byte) 0b1000_1000,
                             STI = (byte) 0b1000_1001,
                             LDSP = (byte) 0b1000_1010,
                             STSP = (byte) 0b1000_1011,
                             LDIMR = (byte) 0b1000_1100,
                             STIMR = (byte) 0b1000_1101,

                             LD = (byte) 0xC0,
                             ST = (byte) 0xC1,
                             ADD = (byte) 0xC2,
                             AND = (byte) 0xC3,
                             OR = (byte) 0xC4,
                             NOT = (byte) 0b1100_0101,
                             SUB = (byte) 0b1100_0110,
                             XOR = (byte) 0b1100_0111,
                             MUL = (byte) 0b1100_1000,
                             CMP = (byte) 0b1100_1001,

                             JMP  = (byte) 0x40,
                             CALL = (byte) 0x41,
                             JGT  = (byte) 0b0100_0010,
                             JGE  = (byte) 0b0100_0011,
                             JLT  = (byte) 0b0100_0100,
                             JLE  = (byte) 0b0100_0101,
                             JEQ  = (byte) 0b0100_0110,
                             JNEQ = (byte) 0b0100_0111,
                             BZ   = (byte) 0b0000_0000;

    // Addressing types
    public static final byte REGDIR = 0,
                             REGIND = 1,
                             REGINDPOM = 2,
                             POSTDECR = 3,
                             IMMED = 4,
                             MEMDIR = 5,
                             MEMIND = 6,
                             PREINCR = 7;

    public static String getInstructionName( byte instruction, boolean isUppercase )
    {
        switch( instruction )
        {
            case (byte) 0x80:  return isUppercase ? "PUSH" : "push";
            case (byte) 0x81:  return isUppercase ? "POP" : "pop";
            case (byte) 0x82:  return isUppercase ? "RET" : "ret";
            case (byte) 0x83:  return isUppercase ? "IRET" : "iret";
            case (byte) 0x84:  return isUppercase ? "HALT" : "halt";
            case (byte) 0x85:  return isUppercase ? "INC" : "inc";
            case (byte) 0x86:  return isUppercase ? "DEC" : "dec";

            case (byte) 0xC0:  return isUppercase ? "LD" : "ld";
            case (byte) 0xC1:  return isUppercase ? "ST" : "st";
            case (byte) 0xC2:  return isUppercase ? "ADD" : "add";
            case (byte) 0xC3:  return isUppercase ? "AND" : "and";
            case (byte) 0xC4:  return isUppercase ? "OR" : "or";
            case (byte) NOT:  return isUppercase ? "NOT" : "not";
            case (byte) XOR:  return isUppercase ? "XOR" : "xor";
            case (byte) SUB:  return isUppercase ? "SUB" : "sub";
            case (byte) MUL:  return isUppercase ? "MUL" : "mul";
            case (byte) CMP:  return isUppercase ? "CMP" : "cmp";

            case (byte) 0x40:  return isUppercase ? "JMP" : "jmp";
            case (byte) 0x41:  return isUppercase ? "CALL" : "call";
            case (byte) BZ:  return isUppercase ? "BZ" : "bz";

            case (byte) CLI:  return isUppercase ? "CLI" : "cli";
            case (byte) STI:  return isUppercase ? "STI" : "sti";
            case (byte) LDSP:  return isUppercase ? "LDSP" : "ldsp";
            case (byte) STSP:  return isUppercase ? "STSP" : "stsp";
            case (byte) LDIMR:  return isUppercase ? "LDIMR" : "ldimr";
            case (byte) STIMR:  return isUppercase ? "STIMR" : "stimr";

            case (byte) JGT:  return isUppercase ? "JGT" : "jgt";
            case (byte) JGE:  return isUppercase ? "JGE" : "jge";
            case (byte) JLT:  return isUppercase ? "JLT" : "jlt";
            case (byte) JLE:  return isUppercase ? "JLE" : "jle";
            case (byte) JEQ:  return isUppercase ? "JEQ" : "jeq";
            case (byte) JNEQ:  return isUppercase ? "JNEQ" : "jneq";
        };
        return "--illegal instruction--";
    }


}
