package assembler;

import java.util.Iterator;
import java.util.LinkedList;

public class RelocationTable {

    private SymbolTable table;
    private LinkedList<RelocationEntry> list;

    public RelocationTable( SymbolTable table )
    {
        this.table = table;
        list = new LinkedList<>();
    }

    public void addRelocationEntry(int addr, SymbolTableElement section, SymbolTableElement symbol, int relocType)
    {
        RelocationEntry en = new RelocationEntry();
        en.addr = addr;
        en.symbol = symbol;
        en.section = section;
        en.relocType = relocType;
        list.add(en);
    }

    public void relocation()
    {
        RelocationEntry e;
        Iterator<RelocationEntry> it = list.iterator();
        while( it.hasNext() )
        {
            e = it.next();
            if ( e.relocType == RelocationEntry.R_PC_abs ) {
                e.section.repairBuffer(e.addr, (short) (e.symbol.addr + e.symbol.section.getSectionAddress()) );
            } else if ( e.relocType == RelocationEntry.R_section )
            {
              short a = e.section.getShortFrom(e.addr);
              a += e.symbol.section.getSectionAddress();
              e.section.repairBuffer(e.addr, a);
            };
        };
    }
}
