package assembler;

import org.antlr.v4.runtime.ParserRuleContext;

public class ErrorHandler {

    public static void print(String error, ParserRuleContext context )
    {
        System.out.println( mssgError(error, context) );
    }

    public static void throwException( String error, ParserRuleContext context )
    {
        throw new RuntimeException( mssgError(error, context) );
    }

    public static void raiseWarning( String warning, ParserRuleContext context )
    {
        System.out.println( mssgWarning(warning, context) );
    }

    private static String mssgError(String error, ParserRuleContext context )
    {
        return "error: " + (context!=null ? context.start.getLine() + ": " : "") + error + ".";
    }

    private static String mssgWarning(String warning, ParserRuleContext context )
    {
        return "warning: " + (context!=null ? context.start.getLine() + ": " : "") + warning + ".";
    }

}
