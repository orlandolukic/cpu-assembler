package assembler;

public class SymbolTableElement {

    public String name;
    public int addr;

    private byte[] buffer;
    private int buffcnt;
    private int sectionAddress;
    public boolean isSection;
    public SymbolTableElement section;
    public int size;
    public boolean isSectionInitialized;

    public SymbolTableElement()
    {
        isSectionInitialized = false;
    }

    public void prepareForSecondPass()
    {
        if ( isSection ) {
            buffer = new byte[size];
            buffcnt = 0;
        };
    }

    public void insertByte(byte b)
    {
        if ( buffer != null )
        {
            buffer[buffcnt++] = b;
        };
    }

    public int getLocationCounter()
    {
        return buffcnt;
    }

    public byte getByteFrom(int addr)
    {
        return buffer != null && addr < size ? buffer[addr] : 0;
    }

    public short getShortFrom(int addr)
    {
        if ( !(buffer != null && addr+1 < size) )
            return 0;

        short v = getByteFrom(addr);
        v |= (getByteFrom(addr+1) << 8);
        return v;
    }

    public void repairBuffer(int addr, byte content)
    {
        if ( buffer != null )
        {
            buffer[addr] = content;
        };
    }

    public void repairBuffer(int addr, short content)
    {
        if ( buffer != null )
        {
            buffer[addr] = (byte) (content & 0xFF);
            buffer[addr+1] = (byte) (((content & 0xFF00) >> 8) & 0xFF);
        };
    }

    public String getSectionContent()
    {
        StringBuilder s = new StringBuilder();
        int m = 0;
        int k = 0;
        s.append("Section content > " + name + "\n");
        if ( buffcnt > 0 ) {
            s.append("(0x00): ");
            for (int i = 0; i < buffcnt; i++, m++) {
                if (i > 0)
                    s.append(" ");
                if (m > 15) {
                    m = 0;
                    k++;
                    s.append("\n(0x" + Integer.toString(k*16,16) + "): ");
                };
                s.append(Functions.getByteAsHexString(buffer[i]));
            };
        } else
        {
            s.append("Section is empty.\n");
        };

        return s.toString();
    }

    public void setSectionAddress( int startAddr )
    {
        if ( !isSection )
            return;
        sectionAddress = startAddr;
    }

    public int getSectionAddress()
    {
        return sectionAddress;
    }

    public void forEachByte(FI_BufferContent bf)
    {
        for (int i=0; i<buffcnt; i++)
            bf.each(buffer[i], i);
    }

}
