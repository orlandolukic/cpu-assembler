package assembler;

public class RelocationEntry {

    public static final int R_PC_rel = 1,
                            R_PC_abs = 2,
                            R_section = 3;

    public int addr;
    public SymbolTableElement section;
    public SymbolTableElement symbol;
    public int relocType;

}
