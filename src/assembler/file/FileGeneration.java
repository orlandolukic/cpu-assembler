package assembler.file;

import assembler.SymbolTable;
import assembler.SymbolTableElement;

import java.io.File;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;

public class FileGeneration {

    private String filename;
    private SymbolTable table;

    public FileGeneration(SymbolTable table, String filename)
    {
        this.table = table;
        this.filename = filename;
    }

    public void generate()
    {
        try {
            File f = new File(filename);
            if ( !f.exists() )
                f.createNewFile();
            PrintWriter pw = new PrintWriter( f );
            pw.println("v2.0 raw");

            SymbolTableElement el;
            LinkedList<SymbolTableElement> sections = table.getAllSections();
            Iterator<SymbolTableElement> it = sections.iterator();
            int startAddr;
            int locCnt = 0;
            boolean added = false;
            while( it.hasNext() )
            {
                el = it.next();
                startAddr = el.getSectionAddress();
                if ( added )
                    pw.print(" ");
                if ( startAddr - locCnt > 0 )
                    pw.print( (startAddr-locCnt) +"*0 " );
                locCnt = startAddr;
                el.forEachByte((b, i) -> {
                    if ( i > 0 )
                        pw.print(" ");
                    pw.print(String.format("%02x", b));
                });
                locCnt += el.size;
                added = true;
            };

            pw.close();
        } catch(Exception e) {

        }
    }
}
