package assembler.file;

import assembler.SymbolTableElement;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class Sorter implements Comparator<SymbolTableElement> {

    @Override
    public int compare(SymbolTableElement o1, SymbolTableElement o2) {
        if ( o1.getSectionAddress() < o2.getSectionAddress() )
            return -1;
        else if ( o1.getSectionAddress() > o2.getSectionAddress() )
            return 1;
        return 0;
    }
}
