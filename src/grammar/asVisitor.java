// Generated from C:/Users/P/Desktop/CPU assembler\as.g4 by ANTLR 4.8
package grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link asParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface asVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link asParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTranslationUnit(asParser.TranslationUnitContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#optionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptionList(asParser.OptionListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code optionDirectiveNotation}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptionDirectiveNotation(asParser.OptionDirectiveNotationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code optionInstructionNotation}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptionInstructionNotation(asParser.OptionInstructionNotationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code optionLabelOnly}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptionLabelOnly(asParser.OptionLabelOnlyContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#labelOnly}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabelOnly(asParser.LabelOnlyContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#directiveNotation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDirectiveNotation(asParser.DirectiveNotationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_rept}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_rept(asParser.D_reptContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_endr}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_endr(asParser.D_endrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_allocation}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_allocation(asParser.D_allocationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_section}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_section(asParser.D_sectionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_byte}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_byte(asParser.D_byteContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_word}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_word(asParser.D_wordContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_skip}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_skip(asParser.D_skipContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_int}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_int(asParser.D_intContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_asciz}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_asciz(asParser.D_ascizContext ctx);
	/**
	 * Visit a parse tree produced by the {@code d_ascii}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_ascii(asParser.D_asciiContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#allocationDirectiveSuffix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllocationDirectiveSuffix(asParser.AllocationDirectiveSuffixContext ctx);
	/**
	 * Visit a parse tree produced by the {@code adsNumber}
	 * labeled alternative in {@link asParser#numberSingle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdsNumber(asParser.AdsNumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code adsLabel}
	 * labeled alternative in {@link asParser#numberSingle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdsLabel(asParser.AdsLabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#instructionNotation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstructionNotation(asParser.InstructionNotationContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#label}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabel(asParser.LabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#labelName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabelName(asParser.LabelNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#instructionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstructionName(asParser.InstructionNameContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_PUSH}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_PUSH(asParser.I_PUSHContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_POP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_POP(asParser.I_POPContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_RET}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_RET(asParser.I_RETContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_IRET}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_IRET(asParser.I_IRETContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_HALT}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_HALT(asParser.I_HALTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_INC}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_INC(asParser.I_INCContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_DEC}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_DEC(asParser.I_DECContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_CLI}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_CLI(asParser.I_CLIContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_STI}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_STI(asParser.I_STIContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_LDSP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_LDSP(asParser.I_LDSPContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_STSP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_STSP(asParser.I_STSPContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_LDIMR}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_LDIMR(asParser.I_LDIMRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code I_STIMR}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_STIMR(asParser.I_STIMRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_LD}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_LD(asParser.IA_LDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_ST}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_ST(asParser.IA_STContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_ADD}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_ADD(asParser.IA_ADDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_AND}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_AND(asParser.IA_ANDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_OR}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_OR(asParser.IA_ORContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_NOT}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_NOT(asParser.IA_NOTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_SUB}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_SUB(asParser.IA_SUBContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_XOR}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_XOR(asParser.IA_XORContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_MUL}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_MUL(asParser.IA_MULContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IA_CMP}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIA_CMP(asParser.IA_CMPContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_CALL}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_CALL(asParser.IB_CALLContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_BZ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_BZ(asParser.IB_BZContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JMP}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JMP(asParser.IB_JMPContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JGT}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JGT(asParser.IB_JGTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JGE}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JGE(asParser.IB_JGEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JLT}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JLT(asParser.IB_JLTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JLE}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JLE(asParser.IB_JLEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JEQ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JEQ(asParser.IB_JEQContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IB_JNEQ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIB_JNEQ(asParser.IB_JNEQContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#addressing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddressing(asParser.AddressingContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#memoryDirectLabel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemoryDirectLabel(asParser.MemoryDirectLabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#plusPlus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusPlus(asParser.PlusPlusContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#minusMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusMinus(asParser.MinusMinusContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#registerIndirect}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegisterIndirect(asParser.RegisterIndirectContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#registerIndirectDisp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegisterIndirectDisp(asParser.RegisterIndirectDispContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#minus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(asParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#memoryDirect}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemoryDirect(asParser.MemoryDirectContext ctx);
	/**
	 * Visit a parse tree produced by {@link asParser#memoryIndirect}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemoryIndirect(asParser.MemoryIndirectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code immediateNumber}
	 * labeled alternative in {@link asParser#immediate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImmediateNumber(asParser.ImmediateNumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code immediateLabel}
	 * labeled alternative in {@link asParser#immediate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImmediateLabel(asParser.ImmediateLabelContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberHex}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberHex(asParser.NumberHexContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberDec}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberDec(asParser.NumberDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberBinary}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberBinary(asParser.NumberBinaryContext ctx);
}