// Generated from C:/Users/P/Desktop/CPU assembler\as.g4 by ANTLR 4.8
package grammar;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class asParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, T__62=63, T__63=64, T__64=65, T__65=66, 
		T__66=67, T__67=68, T__68=69, T__69=70, T__70=71, T__71=72, T__72=73, 
		T__73=74, T__74=75, SectionName=76, LabelLiteral=77, StringLiteral=78, 
		HexValue=79, BinValue=80, NumValue=81, Whitespace=82, Newline=83, BlockComment=84, 
		LineComment=85;
	public static final int
		RULE_translationUnit = 0, RULE_optionList = 1, RULE_optionsEach = 2, RULE_labelOnly = 3, 
		RULE_directiveNotation = 4, RULE_directive = 5, RULE_directiveAllocation = 6, 
		RULE_allocationDirectiveSuffix = 7, RULE_numberSingle = 8, RULE_instructionNotation = 9, 
		RULE_label = 10, RULE_labelName = 11, RULE_instructionName = 12, RULE_addresslessInstructions = 13, 
		RULE_addressInstructions = 14, RULE_branchInstructions = 15, RULE_addressing = 16, 
		RULE_memoryDirectLabel = 17, RULE_plusPlus = 18, RULE_minusMinus = 19, 
		RULE_registerIndirect = 20, RULE_registerIndirectDisp = 21, RULE_minus = 22, 
		RULE_memoryDirect = 23, RULE_memoryIndirect = 24, RULE_immediate = 25, 
		RULE_number = 26;
	private static String[] makeRuleNames() {
		return new String[] {
			"translationUnit", "optionList", "optionsEach", "labelOnly", "directiveNotation", 
			"directive", "directiveAllocation", "allocationDirectiveSuffix", "numberSingle", 
			"instructionNotation", "label", "labelName", "instructionName", "addresslessInstructions", 
			"addressInstructions", "branchInstructions", "addressing", "memoryDirectLabel", 
			"plusPlus", "minusMinus", "registerIndirect", "registerIndirectDisp", 
			"minus", "memoryDirect", "memoryIndirect", "immediate", "number"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "':'", "'.rept'", "'.endr'", "'.section'", "'.byte'", "'.word'", 
			"'.skip'", "'.int'", "'.asciz'", "'.ascii'", "','", "'PUSH'", "'push'", 
			"'POP'", "'pop'", "'RET'", "'ret'", "'IRET'", "'iret'", "'HALT'", "'halt'", 
			"'INC'", "'inc'", "'DEC'", "'dec'", "'CLI'", "'cli'", "'STI'", "'sti'", 
			"'LDSP'", "'ldsp'", "'STSP'", "'stsp'", "'LDIMR'", "'ldimr'", "'STIMR'", 
			"'stimr'", "'LD'", "'ld'", "'ST'", "'st'", "'ADD'", "'add'", "'AND'", 
			"'and'", "'OR'", "'or'", "'NOT'", "'not'", "'SUB'", "'sub'", "'XOR'", 
			"'xor'", "'MUL'", "'mul'", "'CMP'", "'cmp'", "'CALL'", "'call'", "'BZ'", 
			"'bz'", "'JMP'", "'jmp'", "'JGT'", "'JGE'", "'JLT'", "'JLE'", "'JEQ'", 
			"'JNEQ'", "'++'", "'--'", "'['", "']'", "'-'", "'#'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, "SectionName", "LabelLiteral", "StringLiteral", 
			"HexValue", "BinValue", "NumValue", "Whitespace", "Newline", "BlockComment", 
			"LineComment"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "as.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public asParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class TranslationUnitContext extends ParserRuleContext {
		public OptionListContext optionList() {
			return getRuleContext(OptionListContext.class,0);
		}
		public TranslationUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_translationUnit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterTranslationUnit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitTranslationUnit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitTranslationUnit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TranslationUnitContext translationUnit() throws RecognitionException {
		TranslationUnitContext _localctx = new TranslationUnitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_translationUnit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			optionList(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OptionListContext extends ParserRuleContext {
		public OptionsEachContext optionsEach() {
			return getRuleContext(OptionsEachContext.class,0);
		}
		public OptionListContext optionList() {
			return getRuleContext(OptionListContext.class,0);
		}
		public OptionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterOptionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitOptionList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitOptionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OptionListContext optionList() throws RecognitionException {
		return optionList(0);
	}

	private OptionListContext optionList(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		OptionListContext _localctx = new OptionListContext(_ctx, _parentState);
		OptionListContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_optionList, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(57);
			optionsEach();
			}
			_ctx.stop = _input.LT(-1);
			setState(63);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new OptionListContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_optionList);
					setState(59);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(60);
					optionsEach();
					}
					} 
				}
				setState(65);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class OptionsEachContext extends ParserRuleContext {
		public OptionsEachContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optionsEach; }
	 
		public OptionsEachContext() { }
		public void copyFrom(OptionsEachContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OptionDirectiveNotationContext extends OptionsEachContext {
		public DirectiveNotationContext directiveNotation() {
			return getRuleContext(DirectiveNotationContext.class,0);
		}
		public OptionDirectiveNotationContext(OptionsEachContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterOptionDirectiveNotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitOptionDirectiveNotation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitOptionDirectiveNotation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OptionLabelOnlyContext extends OptionsEachContext {
		public LabelOnlyContext labelOnly() {
			return getRuleContext(LabelOnlyContext.class,0);
		}
		public OptionLabelOnlyContext(OptionsEachContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterOptionLabelOnly(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitOptionLabelOnly(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitOptionLabelOnly(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OptionInstructionNotationContext extends OptionsEachContext {
		public InstructionNotationContext instructionNotation() {
			return getRuleContext(InstructionNotationContext.class,0);
		}
		public OptionInstructionNotationContext(OptionsEachContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterOptionInstructionNotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitOptionInstructionNotation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitOptionInstructionNotation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OptionsEachContext optionsEach() throws RecognitionException {
		OptionsEachContext _localctx = new OptionsEachContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_optionsEach);
		try {
			setState(71);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new OptionDirectiveNotationContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(66);
				directiveNotation();
				}
				break;
			case 2:
				_localctx = new OptionInstructionNotationContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(67);
				instructionNotation();
				}
				break;
			case 3:
				_localctx = new OptionLabelOnlyContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(68);
				labelOnly();
				setState(69);
				match(T__0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelOnlyContext extends ParserRuleContext {
		public LabelNameContext labelName() {
			return getRuleContext(LabelNameContext.class,0);
		}
		public LabelOnlyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labelOnly; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterLabelOnly(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitLabelOnly(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitLabelOnly(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LabelOnlyContext labelOnly() throws RecognitionException {
		LabelOnlyContext _localctx = new LabelOnlyContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_labelOnly);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			labelName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectiveNotationContext extends ParserRuleContext {
		public DirectiveContext directive() {
			return getRuleContext(DirectiveContext.class,0);
		}
		public LabelContext label() {
			return getRuleContext(LabelContext.class,0);
		}
		public DirectiveNotationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directiveNotation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterDirectiveNotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitDirectiveNotation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitDirectiveNotation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DirectiveNotationContext directiveNotation() throws RecognitionException {
		DirectiveNotationContext _localctx = new DirectiveNotationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_directiveNotation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LabelLiteral) {
				{
				setState(75);
				label();
				}
			}

			setState(78);
			directive();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectiveContext extends ParserRuleContext {
		public DirectiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directive; }
	 
		public DirectiveContext() { }
		public void copyFrom(DirectiveContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class D_sectionContext extends DirectiveContext {
		public TerminalNode SectionName() { return getToken(asParser.SectionName, 0); }
		public D_sectionContext(DirectiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_section(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_section(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_section(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_reptContext extends DirectiveContext {
		public TerminalNode NumValue() { return getToken(asParser.NumValue, 0); }
		public D_reptContext(DirectiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_rept(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_rept(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_rept(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_endrContext extends DirectiveContext {
		public D_endrContext(DirectiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_endr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_endr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_endr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_allocationContext extends DirectiveContext {
		public DirectiveAllocationContext directiveAllocation() {
			return getRuleContext(DirectiveAllocationContext.class,0);
		}
		public D_allocationContext(DirectiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_allocation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_allocation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_allocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DirectiveContext directive() throws RecognitionException {
		DirectiveContext _localctx = new DirectiveContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_directive);
		try {
			setState(86);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				_localctx = new D_reptContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				match(T__1);
				setState(81);
				match(NumValue);
				}
				break;
			case T__2:
				_localctx = new D_endrContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(82);
				match(T__2);
				}
				break;
			case T__4:
			case T__5:
			case T__6:
			case T__7:
			case T__8:
			case T__9:
				_localctx = new D_allocationContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(83);
				directiveAllocation();
				}
				break;
			case T__3:
				_localctx = new D_sectionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(84);
				match(T__3);
				setState(85);
				match(SectionName);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectiveAllocationContext extends ParserRuleContext {
		public DirectiveAllocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directiveAllocation; }
	 
		public DirectiveAllocationContext() { }
		public void copyFrom(DirectiveAllocationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class D_wordContext extends DirectiveAllocationContext {
		public AllocationDirectiveSuffixContext allocationDirectiveSuffix() {
			return getRuleContext(AllocationDirectiveSuffixContext.class,0);
		}
		public D_wordContext(DirectiveAllocationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_word(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_word(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_word(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_intContext extends DirectiveAllocationContext {
		public AllocationDirectiveSuffixContext allocationDirectiveSuffix() {
			return getRuleContext(AllocationDirectiveSuffixContext.class,0);
		}
		public D_intContext(DirectiveAllocationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_int(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_int(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_int(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_byteContext extends DirectiveAllocationContext {
		public AllocationDirectiveSuffixContext allocationDirectiveSuffix() {
			return getRuleContext(AllocationDirectiveSuffixContext.class,0);
		}
		public D_byteContext(DirectiveAllocationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_byte(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_byte(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_byte(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_asciiContext extends DirectiveAllocationContext {
		public TerminalNode StringLiteral() { return getToken(asParser.StringLiteral, 0); }
		public D_asciiContext(DirectiveAllocationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_ascii(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_ascii(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_ascii(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_skipContext extends DirectiveAllocationContext {
		public TerminalNode NumValue() { return getToken(asParser.NumValue, 0); }
		public D_skipContext(DirectiveAllocationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_skip(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_skip(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_skip(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class D_ascizContext extends DirectiveAllocationContext {
		public TerminalNode StringLiteral() { return getToken(asParser.StringLiteral, 0); }
		public D_ascizContext(DirectiveAllocationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterD_asciz(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitD_asciz(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitD_asciz(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DirectiveAllocationContext directiveAllocation() throws RecognitionException {
		DirectiveAllocationContext _localctx = new DirectiveAllocationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_directiveAllocation);
		try {
			setState(100);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
				_localctx = new D_byteContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(88);
				match(T__4);
				setState(89);
				allocationDirectiveSuffix(0);
				}
				break;
			case T__5:
				_localctx = new D_wordContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(90);
				match(T__5);
				setState(91);
				allocationDirectiveSuffix(0);
				}
				break;
			case T__6:
				_localctx = new D_skipContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(92);
				match(T__6);
				setState(93);
				match(NumValue);
				}
				break;
			case T__7:
				_localctx = new D_intContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(94);
				match(T__7);
				setState(95);
				allocationDirectiveSuffix(0);
				}
				break;
			case T__8:
				_localctx = new D_ascizContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(96);
				match(T__8);
				setState(97);
				match(StringLiteral);
				}
				break;
			case T__9:
				_localctx = new D_asciiContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(98);
				match(T__9);
				setState(99);
				match(StringLiteral);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AllocationDirectiveSuffixContext extends ParserRuleContext {
		public NumberSingleContext numberSingle() {
			return getRuleContext(NumberSingleContext.class,0);
		}
		public AllocationDirectiveSuffixContext allocationDirectiveSuffix() {
			return getRuleContext(AllocationDirectiveSuffixContext.class,0);
		}
		public AllocationDirectiveSuffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allocationDirectiveSuffix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterAllocationDirectiveSuffix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitAllocationDirectiveSuffix(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitAllocationDirectiveSuffix(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AllocationDirectiveSuffixContext allocationDirectiveSuffix() throws RecognitionException {
		return allocationDirectiveSuffix(0);
	}

	private AllocationDirectiveSuffixContext allocationDirectiveSuffix(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AllocationDirectiveSuffixContext _localctx = new AllocationDirectiveSuffixContext(_ctx, _parentState);
		AllocationDirectiveSuffixContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_allocationDirectiveSuffix, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(103);
			numberSingle();
			}
			_ctx.stop = _input.LT(-1);
			setState(110);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AllocationDirectiveSuffixContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_allocationDirectiveSuffix);
					setState(105);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(106);
					match(T__10);
					setState(107);
					numberSingle();
					}
					} 
				}
				setState(112);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class NumberSingleContext extends ParserRuleContext {
		public NumberSingleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numberSingle; }
	 
		public NumberSingleContext() { }
		public void copyFrom(NumberSingleContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AdsNumberContext extends NumberSingleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public AdsNumberContext(NumberSingleContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterAdsNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitAdsNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitAdsNumber(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AdsLabelContext extends NumberSingleContext {
		public LabelNameContext labelName() {
			return getRuleContext(LabelNameContext.class,0);
		}
		public AdsLabelContext(NumberSingleContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterAdsLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitAdsLabel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitAdsLabel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberSingleContext numberSingle() throws RecognitionException {
		NumberSingleContext _localctx = new NumberSingleContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_numberSingle);
		try {
			setState(115);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HexValue:
			case BinValue:
			case NumValue:
				_localctx = new AdsNumberContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(113);
				number();
				}
				break;
			case LabelLiteral:
				_localctx = new AdsLabelContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				labelName();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstructionNotationContext extends ParserRuleContext {
		public InstructionNameContext instructionName() {
			return getRuleContext(InstructionNameContext.class,0);
		}
		public LabelContext label() {
			return getRuleContext(LabelContext.class,0);
		}
		public AddressingContext addressing() {
			return getRuleContext(AddressingContext.class,0);
		}
		public InstructionNotationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instructionNotation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterInstructionNotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitInstructionNotation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitInstructionNotation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstructionNotationContext instructionNotation() throws RecognitionException {
		InstructionNotationContext _localctx = new InstructionNotationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_instructionNotation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LabelLiteral) {
				{
				setState(117);
				label();
				}
			}

			setState(120);
			instructionName();
			setState(122);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				{
				setState(121);
				addressing();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelContext extends ParserRuleContext {
		public LabelNameContext labelName() {
			return getRuleContext(LabelNameContext.class,0);
		}
		public LabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_label; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitLabel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitLabel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LabelContext label() throws RecognitionException {
		LabelContext _localctx = new LabelContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_label);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			labelName();
			setState(125);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelNameContext extends ParserRuleContext {
		public TerminalNode LabelLiteral() { return getToken(asParser.LabelLiteral, 0); }
		public LabelNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labelName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterLabelName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitLabelName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitLabelName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LabelNameContext labelName() throws RecognitionException {
		LabelNameContext _localctx = new LabelNameContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_labelName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(LabelLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstructionNameContext extends ParserRuleContext {
		public AddresslessInstructionsContext addresslessInstructions() {
			return getRuleContext(AddresslessInstructionsContext.class,0);
		}
		public AddressInstructionsContext addressInstructions() {
			return getRuleContext(AddressInstructionsContext.class,0);
		}
		public BranchInstructionsContext branchInstructions() {
			return getRuleContext(BranchInstructionsContext.class,0);
		}
		public InstructionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instructionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterInstructionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitInstructionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitInstructionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstructionNameContext instructionName() throws RecognitionException {
		InstructionNameContext _localctx = new InstructionNameContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_instructionName);
		try {
			setState(132);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
				enterOuterAlt(_localctx, 1);
				{
				setState(129);
				addresslessInstructions();
				}
				break;
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
			case T__47:
			case T__48:
			case T__49:
			case T__50:
			case T__51:
			case T__52:
			case T__53:
			case T__54:
			case T__55:
			case T__56:
				enterOuterAlt(_localctx, 2);
				{
				setState(130);
				addressInstructions();
				}
				break;
			case T__57:
			case T__58:
			case T__59:
			case T__60:
			case T__61:
			case T__62:
			case T__63:
			case T__64:
			case T__65:
			case T__66:
			case T__67:
			case T__68:
				enterOuterAlt(_localctx, 3);
				{
				setState(131);
				branchInstructions();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddresslessInstructionsContext extends ParserRuleContext {
		public AddresslessInstructionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addresslessInstructions; }
	 
		public AddresslessInstructionsContext() { }
		public void copyFrom(AddresslessInstructionsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class I_IRETContext extends AddresslessInstructionsContext {
		public I_IRETContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_IRET(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_IRET(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_IRET(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_PUSHContext extends AddresslessInstructionsContext {
		public I_PUSHContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_PUSH(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_PUSH(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_PUSH(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_STIContext extends AddresslessInstructionsContext {
		public I_STIContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_STI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_STI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_STI(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_STSPContext extends AddresslessInstructionsContext {
		public I_STSPContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_STSP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_STSP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_STSP(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_POPContext extends AddresslessInstructionsContext {
		public I_POPContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_POP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_POP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_POP(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_STIMRContext extends AddresslessInstructionsContext {
		public I_STIMRContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_STIMR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_STIMR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_STIMR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_LDIMRContext extends AddresslessInstructionsContext {
		public I_LDIMRContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_LDIMR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_LDIMR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_LDIMR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_INCContext extends AddresslessInstructionsContext {
		public I_INCContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_INC(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_INC(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_INC(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_HALTContext extends AddresslessInstructionsContext {
		public I_HALTContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_HALT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_HALT(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_HALT(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_RETContext extends AddresslessInstructionsContext {
		public I_RETContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_RET(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_RET(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_RET(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_CLIContext extends AddresslessInstructionsContext {
		public I_CLIContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_CLI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_CLI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_CLI(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_LDSPContext extends AddresslessInstructionsContext {
		public I_LDSPContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_LDSP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_LDSP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_LDSP(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class I_DECContext extends AddresslessInstructionsContext {
		public I_DECContext(AddresslessInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterI_DEC(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitI_DEC(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitI_DEC(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AddresslessInstructionsContext addresslessInstructions() throws RecognitionException {
		AddresslessInstructionsContext _localctx = new AddresslessInstructionsContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_addresslessInstructions);
		int _la;
		try {
			setState(147);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__11:
			case T__12:
				_localctx = new I_PUSHContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(134);
				_la = _input.LA(1);
				if ( !(_la==T__11 || _la==T__12) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__13:
			case T__14:
				_localctx = new I_POPContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(135);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__15:
			case T__16:
				_localctx = new I_RETContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(136);
				_la = _input.LA(1);
				if ( !(_la==T__15 || _la==T__16) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__17:
			case T__18:
				_localctx = new I_IRETContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(137);
				_la = _input.LA(1);
				if ( !(_la==T__17 || _la==T__18) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__19:
			case T__20:
				_localctx = new I_HALTContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(138);
				_la = _input.LA(1);
				if ( !(_la==T__19 || _la==T__20) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__21:
			case T__22:
				_localctx = new I_INCContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(139);
				_la = _input.LA(1);
				if ( !(_la==T__21 || _la==T__22) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__23:
			case T__24:
				_localctx = new I_DECContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(140);
				_la = _input.LA(1);
				if ( !(_la==T__23 || _la==T__24) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__25:
			case T__26:
				_localctx = new I_CLIContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(141);
				_la = _input.LA(1);
				if ( !(_la==T__25 || _la==T__26) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__27:
			case T__28:
				_localctx = new I_STIContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(142);
				_la = _input.LA(1);
				if ( !(_la==T__27 || _la==T__28) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__29:
			case T__30:
				_localctx = new I_LDSPContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(143);
				_la = _input.LA(1);
				if ( !(_la==T__29 || _la==T__30) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__31:
			case T__32:
				_localctx = new I_STSPContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(144);
				_la = _input.LA(1);
				if ( !(_la==T__31 || _la==T__32) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__33:
			case T__34:
				_localctx = new I_LDIMRContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(145);
				_la = _input.LA(1);
				if ( !(_la==T__33 || _la==T__34) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__35:
			case T__36:
				_localctx = new I_STIMRContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(146);
				_la = _input.LA(1);
				if ( !(_la==T__35 || _la==T__36) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddressInstructionsContext extends ParserRuleContext {
		public AddressInstructionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addressInstructions; }
	 
		public AddressInstructionsContext() { }
		public void copyFrom(AddressInstructionsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IA_ANDContext extends AddressInstructionsContext {
		public IA_ANDContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_AND(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_AND(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_AND(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_XORContext extends AddressInstructionsContext {
		public IA_XORContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_XOR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_XOR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_XOR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_ORContext extends AddressInstructionsContext {
		public IA_ORContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_OR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_OR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_OR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_STContext extends AddressInstructionsContext {
		public IA_STContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_ST(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_ST(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_ST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_ADDContext extends AddressInstructionsContext {
		public IA_ADDContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_ADD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_ADD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_ADD(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_SUBContext extends AddressInstructionsContext {
		public IA_SUBContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_SUB(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_SUB(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_SUB(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_LDContext extends AddressInstructionsContext {
		public IA_LDContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_LD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_LD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_LD(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_NOTContext extends AddressInstructionsContext {
		public IA_NOTContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_NOT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_NOT(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_NOT(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_CMPContext extends AddressInstructionsContext {
		public IA_CMPContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_CMP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_CMP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_CMP(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IA_MULContext extends AddressInstructionsContext {
		public IA_MULContext(AddressInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIA_MUL(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIA_MUL(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIA_MUL(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AddressInstructionsContext addressInstructions() throws RecognitionException {
		AddressInstructionsContext _localctx = new AddressInstructionsContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_addressInstructions);
		int _la;
		try {
			setState(159);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__37:
			case T__38:
				_localctx = new IA_LDContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(149);
				_la = _input.LA(1);
				if ( !(_la==T__37 || _la==T__38) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__39:
			case T__40:
				_localctx = new IA_STContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(150);
				_la = _input.LA(1);
				if ( !(_la==T__39 || _la==T__40) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__41:
			case T__42:
				_localctx = new IA_ADDContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(151);
				_la = _input.LA(1);
				if ( !(_la==T__41 || _la==T__42) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__43:
			case T__44:
				_localctx = new IA_ANDContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(152);
				_la = _input.LA(1);
				if ( !(_la==T__43 || _la==T__44) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__45:
			case T__46:
				_localctx = new IA_ORContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(153);
				_la = _input.LA(1);
				if ( !(_la==T__45 || _la==T__46) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__47:
			case T__48:
				_localctx = new IA_NOTContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(154);
				_la = _input.LA(1);
				if ( !(_la==T__47 || _la==T__48) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__49:
			case T__50:
				_localctx = new IA_SUBContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(155);
				_la = _input.LA(1);
				if ( !(_la==T__49 || _la==T__50) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__51:
			case T__52:
				_localctx = new IA_XORContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(156);
				_la = _input.LA(1);
				if ( !(_la==T__51 || _la==T__52) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__53:
			case T__54:
				_localctx = new IA_MULContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(157);
				_la = _input.LA(1);
				if ( !(_la==T__53 || _la==T__54) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__55:
			case T__56:
				_localctx = new IA_CMPContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(158);
				_la = _input.LA(1);
				if ( !(_la==T__55 || _la==T__56) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BranchInstructionsContext extends ParserRuleContext {
		public BranchInstructionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_branchInstructions; }
	 
		public BranchInstructionsContext() { }
		public void copyFrom(BranchInstructionsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IB_JEQContext extends BranchInstructionsContext {
		public IB_JEQContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JEQ(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JEQ(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JEQ(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_JGTContext extends BranchInstructionsContext {
		public IB_JGTContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JGT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JGT(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JGT(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_JGEContext extends BranchInstructionsContext {
		public IB_JGEContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JGE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JGE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JGE(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_JNEQContext extends BranchInstructionsContext {
		public IB_JNEQContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JNEQ(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JNEQ(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JNEQ(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_CALLContext extends BranchInstructionsContext {
		public IB_CALLContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_CALL(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_CALL(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_CALL(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_JMPContext extends BranchInstructionsContext {
		public IB_JMPContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JMP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JMP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JMP(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_BZContext extends BranchInstructionsContext {
		public IB_BZContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_BZ(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_BZ(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_BZ(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_JLTContext extends BranchInstructionsContext {
		public IB_JLTContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JLT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JLT(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JLT(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IB_JLEContext extends BranchInstructionsContext {
		public IB_JLEContext(BranchInstructionsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterIB_JLE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitIB_JLE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitIB_JLE(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BranchInstructionsContext branchInstructions() throws RecognitionException {
		BranchInstructionsContext _localctx = new BranchInstructionsContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_branchInstructions);
		int _la;
		try {
			setState(170);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				_localctx = new IB_CALLContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(161);
				_la = _input.LA(1);
				if ( !(_la==T__57 || _la==T__58) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 2:
				_localctx = new IB_BZContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(162);
				_la = _input.LA(1);
				if ( !(_la==T__59 || _la==T__60) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 3:
				_localctx = new IB_JMPContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(163);
				_la = _input.LA(1);
				if ( !(_la==T__61 || _la==T__62) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 4:
				_localctx = new IB_JGTContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(164);
				_la = _input.LA(1);
				if ( !(_la==T__60 || _la==T__63) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 5:
				_localctx = new IB_JGEContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(165);
				_la = _input.LA(1);
				if ( !(_la==T__60 || _la==T__64) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 6:
				_localctx = new IB_JLTContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(166);
				_la = _input.LA(1);
				if ( !(_la==T__60 || _la==T__65) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 7:
				_localctx = new IB_JLEContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(167);
				_la = _input.LA(1);
				if ( !(_la==T__60 || _la==T__66) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 8:
				_localctx = new IB_JEQContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(168);
				_la = _input.LA(1);
				if ( !(_la==T__60 || _la==T__67) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 9:
				_localctx = new IB_JNEQContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(169);
				_la = _input.LA(1);
				if ( !(_la==T__60 || _la==T__68) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddressingContext extends ParserRuleContext {
		public RegisterIndirectContext registerIndirect() {
			return getRuleContext(RegisterIndirectContext.class,0);
		}
		public MemoryDirectContext memoryDirect() {
			return getRuleContext(MemoryDirectContext.class,0);
		}
		public MemoryIndirectContext memoryIndirect() {
			return getRuleContext(MemoryIndirectContext.class,0);
		}
		public ImmediateContext immediate() {
			return getRuleContext(ImmediateContext.class,0);
		}
		public MemoryDirectLabelContext memoryDirectLabel() {
			return getRuleContext(MemoryDirectLabelContext.class,0);
		}
		public AddressingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addressing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterAddressing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitAddressing(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitAddressing(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AddressingContext addressing() throws RecognitionException {
		AddressingContext _localctx = new AddressingContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_addressing);
		try {
			setState(177);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(172);
				registerIndirect();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(173);
				memoryDirect();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(174);
				memoryIndirect();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(175);
				immediate();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(176);
				memoryDirectLabel();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MemoryDirectLabelContext extends ParserRuleContext {
		public LabelNameContext labelName() {
			return getRuleContext(LabelNameContext.class,0);
		}
		public PlusPlusContext plusPlus() {
			return getRuleContext(PlusPlusContext.class,0);
		}
		public MinusMinusContext minusMinus() {
			return getRuleContext(MinusMinusContext.class,0);
		}
		public MemoryDirectLabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_memoryDirectLabel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterMemoryDirectLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitMemoryDirectLabel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitMemoryDirectLabel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MemoryDirectLabelContext memoryDirectLabel() throws RecognitionException {
		MemoryDirectLabelContext _localctx = new MemoryDirectLabelContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_memoryDirectLabel);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__69) {
				{
				setState(179);
				plusPlus();
				}
			}

			setState(182);
			labelName();
			setState(184);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(183);
				minusMinus();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusPlusContext extends ParserRuleContext {
		public PlusPlusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plusPlus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterPlusPlus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitPlusPlus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitPlusPlus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlusPlusContext plusPlus() throws RecognitionException {
		PlusPlusContext _localctx = new PlusPlusContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_plusPlus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			match(T__69);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MinusMinusContext extends ParserRuleContext {
		public MinusMinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_minusMinus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterMinusMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitMinusMinus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitMinusMinus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MinusMinusContext minusMinus() throws RecognitionException {
		MinusMinusContext _localctx = new MinusMinusContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_minusMinus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			match(T__70);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegisterIndirectContext extends ParserRuleContext {
		public TerminalNode LabelLiteral() { return getToken(asParser.LabelLiteral, 0); }
		public RegisterIndirectDispContext registerIndirectDisp() {
			return getRuleContext(RegisterIndirectDispContext.class,0);
		}
		public RegisterIndirectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_registerIndirect; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterRegisterIndirect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitRegisterIndirect(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitRegisterIndirect(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegisterIndirectContext registerIndirect() throws RecognitionException {
		RegisterIndirectContext _localctx = new RegisterIndirectContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_registerIndirect);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			match(T__71);
			setState(191);
			match(LabelLiteral);
			setState(192);
			match(T__72);
			setState(194);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(193);
				registerIndirectDisp();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegisterIndirectDispContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public MinusContext minus() {
			return getRuleContext(MinusContext.class,0);
		}
		public RegisterIndirectDispContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_registerIndirectDisp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterRegisterIndirectDisp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitRegisterIndirectDisp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitRegisterIndirectDisp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegisterIndirectDispContext registerIndirectDisp() throws RecognitionException {
		RegisterIndirectDispContext _localctx = new RegisterIndirectDispContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_registerIndirectDisp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			match(T__10);
			setState(198);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__73) {
				{
				setState(197);
				minus();
				}
			}

			setState(200);
			number();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MinusContext extends ParserRuleContext {
		public MinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_minus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitMinus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitMinus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MinusContext minus() throws RecognitionException {
		MinusContext _localctx = new MinusContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_minus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(202);
			match(T__73);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MemoryDirectContext extends ParserRuleContext {
		public TerminalNode HexValue() { return getToken(asParser.HexValue, 0); }
		public MemoryDirectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_memoryDirect; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterMemoryDirect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitMemoryDirect(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitMemoryDirect(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MemoryDirectContext memoryDirect() throws RecognitionException {
		MemoryDirectContext _localctx = new MemoryDirectContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_memoryDirect);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(204);
			match(HexValue);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MemoryIndirectContext extends ParserRuleContext {
		public TerminalNode HexValue() { return getToken(asParser.HexValue, 0); }
		public MemoryIndirectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_memoryIndirect; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterMemoryIndirect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitMemoryIndirect(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitMemoryIndirect(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MemoryIndirectContext memoryIndirect() throws RecognitionException {
		MemoryIndirectContext _localctx = new MemoryIndirectContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_memoryIndirect);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			match(T__71);
			setState(207);
			match(HexValue);
			setState(208);
			match(T__72);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImmediateContext extends ParserRuleContext {
		public ImmediateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_immediate; }
	 
		public ImmediateContext() { }
		public void copyFrom(ImmediateContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ImmediateNumberContext extends ImmediateContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public ImmediateNumberContext(ImmediateContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterImmediateNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitImmediateNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitImmediateNumber(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImmediateLabelContext extends ImmediateContext {
		public TerminalNode LabelLiteral() { return getToken(asParser.LabelLiteral, 0); }
		public ImmediateLabelContext(ImmediateContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterImmediateLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitImmediateLabel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitImmediateLabel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImmediateContext immediate() throws RecognitionException {
		ImmediateContext _localctx = new ImmediateContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_immediate);
		try {
			setState(214);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				_localctx = new ImmediateNumberContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(210);
				match(T__74);
				setState(211);
				number();
				}
				break;
			case 2:
				_localctx = new ImmediateLabelContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(212);
				match(T__74);
				setState(213);
				match(LabelLiteral);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
	 
		public NumberContext() { }
		public void copyFrom(NumberContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NumberBinaryContext extends NumberContext {
		public TerminalNode BinValue() { return getToken(asParser.BinValue, 0); }
		public NumberBinaryContext(NumberContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterNumberBinary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitNumberBinary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitNumberBinary(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberHexContext extends NumberContext {
		public TerminalNode HexValue() { return getToken(asParser.HexValue, 0); }
		public NumberHexContext(NumberContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterNumberHex(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitNumberHex(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitNumberHex(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberDecContext extends NumberContext {
		public TerminalNode NumValue() { return getToken(asParser.NumValue, 0); }
		public NumberDecContext(NumberContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).enterNumberDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof asListener ) ((asListener)listener).exitNumberDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof asVisitor ) return ((asVisitor<? extends T>)visitor).visitNumberDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_number);
		try {
			setState(219);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HexValue:
				_localctx = new NumberHexContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(216);
				match(HexValue);
				}
				break;
			case NumValue:
				_localctx = new NumberDecContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(217);
				match(NumValue);
				}
				break;
			case BinValue:
				_localctx = new NumberBinaryContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(218);
				match(BinValue);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return optionList_sempred((OptionListContext)_localctx, predIndex);
		case 7:
			return allocationDirectiveSuffix_sempred((AllocationDirectiveSuffixContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean optionList_sempred(OptionListContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean allocationDirectiveSuffix_sempred(AllocationDirectiveSuffixContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3W\u00e0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3@\n\3\f"+
		"\3\16\3C\13\3\3\4\3\4\3\4\3\4\3\4\5\4J\n\4\3\5\3\5\3\6\5\6O\n\6\3\6\3"+
		"\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7Y\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\5\bg\n\b\3\t\3\t\3\t\3\t\3\t\3\t\7\to\n\t\f\t\16\tr\13"+
		"\t\3\n\3\n\5\nv\n\n\3\13\5\13y\n\13\3\13\3\13\5\13}\n\13\3\f\3\f\3\f\3"+
		"\r\3\r\3\16\3\16\3\16\5\16\u0087\n\16\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0096\n\17\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00a2\n\20\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\5\21\u00ad\n\21\3\22\3\22\3\22\3\22\3\22\5\22\u00b4"+
		"\n\22\3\23\5\23\u00b7\n\23\3\23\3\23\5\23\u00bb\n\23\3\24\3\24\3\25\3"+
		"\25\3\26\3\26\3\26\3\26\5\26\u00c5\n\26\3\27\3\27\5\27\u00c9\n\27\3\27"+
		"\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\5\33"+
		"\u00d9\n\33\3\34\3\34\3\34\5\34\u00de\n\34\3\34\2\4\4\20\35\2\4\6\b\n"+
		"\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\66\2\"\3\2\16\17\3\2\20"+
		"\21\3\2\22\23\3\2\24\25\3\2\26\27\3\2\30\31\3\2\32\33\3\2\34\35\3\2\36"+
		"\37\3\2 !\3\2\"#\3\2$%\3\2&\'\3\2()\3\2*+\3\2,-\3\2./\3\2\60\61\3\2\62"+
		"\63\3\2\64\65\3\2\66\67\3\289\3\2:;\3\2<=\3\2>?\3\2@A\4\2??BB\4\2??CC"+
		"\4\2??DD\4\2??EE\4\2??FF\4\2??GG\2\u00fe\28\3\2\2\2\4:\3\2\2\2\6I\3\2"+
		"\2\2\bK\3\2\2\2\nN\3\2\2\2\fX\3\2\2\2\16f\3\2\2\2\20h\3\2\2\2\22u\3\2"+
		"\2\2\24x\3\2\2\2\26~\3\2\2\2\30\u0081\3\2\2\2\32\u0086\3\2\2\2\34\u0095"+
		"\3\2\2\2\36\u00a1\3\2\2\2 \u00ac\3\2\2\2\"\u00b3\3\2\2\2$\u00b6\3\2\2"+
		"\2&\u00bc\3\2\2\2(\u00be\3\2\2\2*\u00c0\3\2\2\2,\u00c6\3\2\2\2.\u00cc"+
		"\3\2\2\2\60\u00ce\3\2\2\2\62\u00d0\3\2\2\2\64\u00d8\3\2\2\2\66\u00dd\3"+
		"\2\2\289\5\4\3\29\3\3\2\2\2:;\b\3\1\2;<\5\6\4\2<A\3\2\2\2=>\f\3\2\2>@"+
		"\5\6\4\2?=\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2B\5\3\2\2\2CA\3\2\2\2"+
		"DJ\5\n\6\2EJ\5\24\13\2FG\5\b\5\2GH\7\3\2\2HJ\3\2\2\2ID\3\2\2\2IE\3\2\2"+
		"\2IF\3\2\2\2J\7\3\2\2\2KL\5\30\r\2L\t\3\2\2\2MO\5\26\f\2NM\3\2\2\2NO\3"+
		"\2\2\2OP\3\2\2\2PQ\5\f\7\2Q\13\3\2\2\2RS\7\4\2\2SY\7S\2\2TY\7\5\2\2UY"+
		"\5\16\b\2VW\7\6\2\2WY\7N\2\2XR\3\2\2\2XT\3\2\2\2XU\3\2\2\2XV\3\2\2\2Y"+
		"\r\3\2\2\2Z[\7\7\2\2[g\5\20\t\2\\]\7\b\2\2]g\5\20\t\2^_\7\t\2\2_g\7S\2"+
		"\2`a\7\n\2\2ag\5\20\t\2bc\7\13\2\2cg\7P\2\2de\7\f\2\2eg\7P\2\2fZ\3\2\2"+
		"\2f\\\3\2\2\2f^\3\2\2\2f`\3\2\2\2fb\3\2\2\2fd\3\2\2\2g\17\3\2\2\2hi\b"+
		"\t\1\2ij\5\22\n\2jp\3\2\2\2kl\f\3\2\2lm\7\r\2\2mo\5\22\n\2nk\3\2\2\2o"+
		"r\3\2\2\2pn\3\2\2\2pq\3\2\2\2q\21\3\2\2\2rp\3\2\2\2sv\5\66\34\2tv\5\30"+
		"\r\2us\3\2\2\2ut\3\2\2\2v\23\3\2\2\2wy\5\26\f\2xw\3\2\2\2xy\3\2\2\2yz"+
		"\3\2\2\2z|\5\32\16\2{}\5\"\22\2|{\3\2\2\2|}\3\2\2\2}\25\3\2\2\2~\177\5"+
		"\30\r\2\177\u0080\7\3\2\2\u0080\27\3\2\2\2\u0081\u0082\7O\2\2\u0082\31"+
		"\3\2\2\2\u0083\u0087\5\34\17\2\u0084\u0087\5\36\20\2\u0085\u0087\5 \21"+
		"\2\u0086\u0083\3\2\2\2\u0086\u0084\3\2\2\2\u0086\u0085\3\2\2\2\u0087\33"+
		"\3\2\2\2\u0088\u0096\t\2\2\2\u0089\u0096\t\3\2\2\u008a\u0096\t\4\2\2\u008b"+
		"\u0096\t\5\2\2\u008c\u0096\t\6\2\2\u008d\u0096\t\7\2\2\u008e\u0096\t\b"+
		"\2\2\u008f\u0096\t\t\2\2\u0090\u0096\t\n\2\2\u0091\u0096\t\13\2\2\u0092"+
		"\u0096\t\f\2\2\u0093\u0096\t\r\2\2\u0094\u0096\t\16\2\2\u0095\u0088\3"+
		"\2\2\2\u0095\u0089\3\2\2\2\u0095\u008a\3\2\2\2\u0095\u008b\3\2\2\2\u0095"+
		"\u008c\3\2\2\2\u0095\u008d\3\2\2\2\u0095\u008e\3\2\2\2\u0095\u008f\3\2"+
		"\2\2\u0095\u0090\3\2\2\2\u0095\u0091\3\2\2\2\u0095\u0092\3\2\2\2\u0095"+
		"\u0093\3\2\2\2\u0095\u0094\3\2\2\2\u0096\35\3\2\2\2\u0097\u00a2\t\17\2"+
		"\2\u0098\u00a2\t\20\2\2\u0099\u00a2\t\21\2\2\u009a\u00a2\t\22\2\2\u009b"+
		"\u00a2\t\23\2\2\u009c\u00a2\t\24\2\2\u009d\u00a2\t\25\2\2\u009e\u00a2"+
		"\t\26\2\2\u009f\u00a2\t\27\2\2\u00a0\u00a2\t\30\2\2\u00a1\u0097\3\2\2"+
		"\2\u00a1\u0098\3\2\2\2\u00a1\u0099\3\2\2\2\u00a1\u009a\3\2\2\2\u00a1\u009b"+
		"\3\2\2\2\u00a1\u009c\3\2\2\2\u00a1\u009d\3\2\2\2\u00a1\u009e\3\2\2\2\u00a1"+
		"\u009f\3\2\2\2\u00a1\u00a0\3\2\2\2\u00a2\37\3\2\2\2\u00a3\u00ad\t\31\2"+
		"\2\u00a4\u00ad\t\32\2\2\u00a5\u00ad\t\33\2\2\u00a6\u00ad\t\34\2\2\u00a7"+
		"\u00ad\t\35\2\2\u00a8\u00ad\t\36\2\2\u00a9\u00ad\t\37\2\2\u00aa\u00ad"+
		"\t \2\2\u00ab\u00ad\t!\2\2\u00ac\u00a3\3\2\2\2\u00ac\u00a4\3\2\2\2\u00ac"+
		"\u00a5\3\2\2\2\u00ac\u00a6\3\2\2\2\u00ac\u00a7\3\2\2\2\u00ac\u00a8\3\2"+
		"\2\2\u00ac\u00a9\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ac\u00ab\3\2\2\2\u00ad"+
		"!\3\2\2\2\u00ae\u00b4\5*\26\2\u00af\u00b4\5\60\31\2\u00b0\u00b4\5\62\32"+
		"\2\u00b1\u00b4\5\64\33\2\u00b2\u00b4\5$\23\2\u00b3\u00ae\3\2\2\2\u00b3"+
		"\u00af\3\2\2\2\u00b3\u00b0\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b2\3\2"+
		"\2\2\u00b4#\3\2\2\2\u00b5\u00b7\5&\24\2\u00b6\u00b5\3\2\2\2\u00b6\u00b7"+
		"\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00ba\5\30\r\2\u00b9\u00bb\5(\25\2"+
		"\u00ba\u00b9\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb%\3\2\2\2\u00bc\u00bd\7"+
		"H\2\2\u00bd\'\3\2\2\2\u00be\u00bf\7I\2\2\u00bf)\3\2\2\2\u00c0\u00c1\7"+
		"J\2\2\u00c1\u00c2\7O\2\2\u00c2\u00c4\7K\2\2\u00c3\u00c5\5,\27\2\u00c4"+
		"\u00c3\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5+\3\2\2\2\u00c6\u00c8\7\r\2\2"+
		"\u00c7\u00c9\5.\30\2\u00c8\u00c7\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ca"+
		"\3\2\2\2\u00ca\u00cb\5\66\34\2\u00cb-\3\2\2\2\u00cc\u00cd\7L\2\2\u00cd"+
		"/\3\2\2\2\u00ce\u00cf\7Q\2\2\u00cf\61\3\2\2\2\u00d0\u00d1\7J\2\2\u00d1"+
		"\u00d2\7Q\2\2\u00d2\u00d3\7K\2\2\u00d3\63\3\2\2\2\u00d4\u00d5\7M\2\2\u00d5"+
		"\u00d9\5\66\34\2\u00d6\u00d7\7M\2\2\u00d7\u00d9\7O\2\2\u00d8\u00d4\3\2"+
		"\2\2\u00d8\u00d6\3\2\2\2\u00d9\65\3\2\2\2\u00da\u00de\7Q\2\2\u00db\u00de"+
		"\7S\2\2\u00dc\u00de\7R\2\2\u00dd\u00da\3\2\2\2\u00dd\u00db\3\2\2\2\u00dd"+
		"\u00dc\3\2\2\2\u00de\67\3\2\2\2\26AINXfpux|\u0086\u0095\u00a1\u00ac\u00b3"+
		"\u00b6\u00ba\u00c4\u00c8\u00d8\u00dd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}