// Generated from C:/Users/P/Desktop/CPU assembler\as.g4 by ANTLR 4.8
package grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link asParser}.
 */
public interface asListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link asParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterTranslationUnit(asParser.TranslationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitTranslationUnit(asParser.TranslationUnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#optionList}.
	 * @param ctx the parse tree
	 */
	void enterOptionList(asParser.OptionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#optionList}.
	 * @param ctx the parse tree
	 */
	void exitOptionList(asParser.OptionListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code optionDirectiveNotation}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 */
	void enterOptionDirectiveNotation(asParser.OptionDirectiveNotationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code optionDirectiveNotation}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 */
	void exitOptionDirectiveNotation(asParser.OptionDirectiveNotationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code optionInstructionNotation}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 */
	void enterOptionInstructionNotation(asParser.OptionInstructionNotationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code optionInstructionNotation}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 */
	void exitOptionInstructionNotation(asParser.OptionInstructionNotationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code optionLabelOnly}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 */
	void enterOptionLabelOnly(asParser.OptionLabelOnlyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code optionLabelOnly}
	 * labeled alternative in {@link asParser#optionsEach}.
	 * @param ctx the parse tree
	 */
	void exitOptionLabelOnly(asParser.OptionLabelOnlyContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#labelOnly}.
	 * @param ctx the parse tree
	 */
	void enterLabelOnly(asParser.LabelOnlyContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#labelOnly}.
	 * @param ctx the parse tree
	 */
	void exitLabelOnly(asParser.LabelOnlyContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#directiveNotation}.
	 * @param ctx the parse tree
	 */
	void enterDirectiveNotation(asParser.DirectiveNotationContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#directiveNotation}.
	 * @param ctx the parse tree
	 */
	void exitDirectiveNotation(asParser.DirectiveNotationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_rept}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void enterD_rept(asParser.D_reptContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_rept}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void exitD_rept(asParser.D_reptContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_endr}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void enterD_endr(asParser.D_endrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_endr}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void exitD_endr(asParser.D_endrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_allocation}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void enterD_allocation(asParser.D_allocationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_allocation}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void exitD_allocation(asParser.D_allocationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_section}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void enterD_section(asParser.D_sectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_section}
	 * labeled alternative in {@link asParser#directive}.
	 * @param ctx the parse tree
	 */
	void exitD_section(asParser.D_sectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_byte}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void enterD_byte(asParser.D_byteContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_byte}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void exitD_byte(asParser.D_byteContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_word}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void enterD_word(asParser.D_wordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_word}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void exitD_word(asParser.D_wordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_skip}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void enterD_skip(asParser.D_skipContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_skip}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void exitD_skip(asParser.D_skipContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_int}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void enterD_int(asParser.D_intContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_int}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void exitD_int(asParser.D_intContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_asciz}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void enterD_asciz(asParser.D_ascizContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_asciz}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void exitD_asciz(asParser.D_ascizContext ctx);
	/**
	 * Enter a parse tree produced by the {@code d_ascii}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void enterD_ascii(asParser.D_asciiContext ctx);
	/**
	 * Exit a parse tree produced by the {@code d_ascii}
	 * labeled alternative in {@link asParser#directiveAllocation}.
	 * @param ctx the parse tree
	 */
	void exitD_ascii(asParser.D_asciiContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#allocationDirectiveSuffix}.
	 * @param ctx the parse tree
	 */
	void enterAllocationDirectiveSuffix(asParser.AllocationDirectiveSuffixContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#allocationDirectiveSuffix}.
	 * @param ctx the parse tree
	 */
	void exitAllocationDirectiveSuffix(asParser.AllocationDirectiveSuffixContext ctx);
	/**
	 * Enter a parse tree produced by the {@code adsNumber}
	 * labeled alternative in {@link asParser#numberSingle}.
	 * @param ctx the parse tree
	 */
	void enterAdsNumber(asParser.AdsNumberContext ctx);
	/**
	 * Exit a parse tree produced by the {@code adsNumber}
	 * labeled alternative in {@link asParser#numberSingle}.
	 * @param ctx the parse tree
	 */
	void exitAdsNumber(asParser.AdsNumberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code adsLabel}
	 * labeled alternative in {@link asParser#numberSingle}.
	 * @param ctx the parse tree
	 */
	void enterAdsLabel(asParser.AdsLabelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code adsLabel}
	 * labeled alternative in {@link asParser#numberSingle}.
	 * @param ctx the parse tree
	 */
	void exitAdsLabel(asParser.AdsLabelContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#instructionNotation}.
	 * @param ctx the parse tree
	 */
	void enterInstructionNotation(asParser.InstructionNotationContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#instructionNotation}.
	 * @param ctx the parse tree
	 */
	void exitInstructionNotation(asParser.InstructionNotationContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#label}.
	 * @param ctx the parse tree
	 */
	void enterLabel(asParser.LabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#label}.
	 * @param ctx the parse tree
	 */
	void exitLabel(asParser.LabelContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#labelName}.
	 * @param ctx the parse tree
	 */
	void enterLabelName(asParser.LabelNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#labelName}.
	 * @param ctx the parse tree
	 */
	void exitLabelName(asParser.LabelNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#instructionName}.
	 * @param ctx the parse tree
	 */
	void enterInstructionName(asParser.InstructionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#instructionName}.
	 * @param ctx the parse tree
	 */
	void exitInstructionName(asParser.InstructionNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_PUSH}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_PUSH(asParser.I_PUSHContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_PUSH}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_PUSH(asParser.I_PUSHContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_POP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_POP(asParser.I_POPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_POP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_POP(asParser.I_POPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_RET}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_RET(asParser.I_RETContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_RET}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_RET(asParser.I_RETContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_IRET}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_IRET(asParser.I_IRETContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_IRET}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_IRET(asParser.I_IRETContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_HALT}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_HALT(asParser.I_HALTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_HALT}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_HALT(asParser.I_HALTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_INC}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_INC(asParser.I_INCContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_INC}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_INC(asParser.I_INCContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_DEC}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_DEC(asParser.I_DECContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_DEC}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_DEC(asParser.I_DECContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_CLI}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_CLI(asParser.I_CLIContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_CLI}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_CLI(asParser.I_CLIContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_STI}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_STI(asParser.I_STIContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_STI}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_STI(asParser.I_STIContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_LDSP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_LDSP(asParser.I_LDSPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_LDSP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_LDSP(asParser.I_LDSPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_STSP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_STSP(asParser.I_STSPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_STSP}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_STSP(asParser.I_STSPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_LDIMR}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_LDIMR(asParser.I_LDIMRContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_LDIMR}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_LDIMR(asParser.I_LDIMRContext ctx);
	/**
	 * Enter a parse tree produced by the {@code I_STIMR}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void enterI_STIMR(asParser.I_STIMRContext ctx);
	/**
	 * Exit a parse tree produced by the {@code I_STIMR}
	 * labeled alternative in {@link asParser#addresslessInstructions}.
	 * @param ctx the parse tree
	 */
	void exitI_STIMR(asParser.I_STIMRContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_LD}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_LD(asParser.IA_LDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_LD}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_LD(asParser.IA_LDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_ST}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_ST(asParser.IA_STContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_ST}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_ST(asParser.IA_STContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_ADD}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_ADD(asParser.IA_ADDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_ADD}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_ADD(asParser.IA_ADDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_AND}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_AND(asParser.IA_ANDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_AND}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_AND(asParser.IA_ANDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_OR}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_OR(asParser.IA_ORContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_OR}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_OR(asParser.IA_ORContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_NOT}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_NOT(asParser.IA_NOTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_NOT}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_NOT(asParser.IA_NOTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_SUB}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_SUB(asParser.IA_SUBContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_SUB}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_SUB(asParser.IA_SUBContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_XOR}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_XOR(asParser.IA_XORContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_XOR}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_XOR(asParser.IA_XORContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_MUL}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_MUL(asParser.IA_MULContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_MUL}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_MUL(asParser.IA_MULContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IA_CMP}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIA_CMP(asParser.IA_CMPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IA_CMP}
	 * labeled alternative in {@link asParser#addressInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIA_CMP(asParser.IA_CMPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_CALL}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_CALL(asParser.IB_CALLContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_CALL}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_CALL(asParser.IB_CALLContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_BZ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_BZ(asParser.IB_BZContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_BZ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_BZ(asParser.IB_BZContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JMP}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JMP(asParser.IB_JMPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JMP}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JMP(asParser.IB_JMPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JGT}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JGT(asParser.IB_JGTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JGT}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JGT(asParser.IB_JGTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JGE}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JGE(asParser.IB_JGEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JGE}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JGE(asParser.IB_JGEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JLT}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JLT(asParser.IB_JLTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JLT}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JLT(asParser.IB_JLTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JLE}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JLE(asParser.IB_JLEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JLE}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JLE(asParser.IB_JLEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JEQ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JEQ(asParser.IB_JEQContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JEQ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JEQ(asParser.IB_JEQContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IB_JNEQ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void enterIB_JNEQ(asParser.IB_JNEQContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IB_JNEQ}
	 * labeled alternative in {@link asParser#branchInstructions}.
	 * @param ctx the parse tree
	 */
	void exitIB_JNEQ(asParser.IB_JNEQContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#addressing}.
	 * @param ctx the parse tree
	 */
	void enterAddressing(asParser.AddressingContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#addressing}.
	 * @param ctx the parse tree
	 */
	void exitAddressing(asParser.AddressingContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#memoryDirectLabel}.
	 * @param ctx the parse tree
	 */
	void enterMemoryDirectLabel(asParser.MemoryDirectLabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#memoryDirectLabel}.
	 * @param ctx the parse tree
	 */
	void exitMemoryDirectLabel(asParser.MemoryDirectLabelContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#plusPlus}.
	 * @param ctx the parse tree
	 */
	void enterPlusPlus(asParser.PlusPlusContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#plusPlus}.
	 * @param ctx the parse tree
	 */
	void exitPlusPlus(asParser.PlusPlusContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#minusMinus}.
	 * @param ctx the parse tree
	 */
	void enterMinusMinus(asParser.MinusMinusContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#minusMinus}.
	 * @param ctx the parse tree
	 */
	void exitMinusMinus(asParser.MinusMinusContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#registerIndirect}.
	 * @param ctx the parse tree
	 */
	void enterRegisterIndirect(asParser.RegisterIndirectContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#registerIndirect}.
	 * @param ctx the parse tree
	 */
	void exitRegisterIndirect(asParser.RegisterIndirectContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#registerIndirectDisp}.
	 * @param ctx the parse tree
	 */
	void enterRegisterIndirectDisp(asParser.RegisterIndirectDispContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#registerIndirectDisp}.
	 * @param ctx the parse tree
	 */
	void exitRegisterIndirectDisp(asParser.RegisterIndirectDispContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#minus}.
	 * @param ctx the parse tree
	 */
	void enterMinus(asParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#minus}.
	 * @param ctx the parse tree
	 */
	void exitMinus(asParser.MinusContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#memoryDirect}.
	 * @param ctx the parse tree
	 */
	void enterMemoryDirect(asParser.MemoryDirectContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#memoryDirect}.
	 * @param ctx the parse tree
	 */
	void exitMemoryDirect(asParser.MemoryDirectContext ctx);
	/**
	 * Enter a parse tree produced by {@link asParser#memoryIndirect}.
	 * @param ctx the parse tree
	 */
	void enterMemoryIndirect(asParser.MemoryIndirectContext ctx);
	/**
	 * Exit a parse tree produced by {@link asParser#memoryIndirect}.
	 * @param ctx the parse tree
	 */
	void exitMemoryIndirect(asParser.MemoryIndirectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code immediateNumber}
	 * labeled alternative in {@link asParser#immediate}.
	 * @param ctx the parse tree
	 */
	void enterImmediateNumber(asParser.ImmediateNumberContext ctx);
	/**
	 * Exit a parse tree produced by the {@code immediateNumber}
	 * labeled alternative in {@link asParser#immediate}.
	 * @param ctx the parse tree
	 */
	void exitImmediateNumber(asParser.ImmediateNumberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code immediateLabel}
	 * labeled alternative in {@link asParser#immediate}.
	 * @param ctx the parse tree
	 */
	void enterImmediateLabel(asParser.ImmediateLabelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code immediateLabel}
	 * labeled alternative in {@link asParser#immediate}.
	 * @param ctx the parse tree
	 */
	void exitImmediateLabel(asParser.ImmediateLabelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberHex}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumberHex(asParser.NumberHexContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberHex}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumberHex(asParser.NumberHexContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberDec}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumberDec(asParser.NumberDecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberDec}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumberDec(asParser.NumberDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberBinary}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumberBinary(asParser.NumberBinaryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberBinary}
	 * labeled alternative in {@link asParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumberBinary(asParser.NumberBinaryContext ctx);
}