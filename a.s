
.section .text

    LD #0b1000
    ST r1

l:
    LD r1
    DEC
    ST r1
    CMP #0
    JEQ kraj
    JMP l

kraj:
    HALT


defaultHandler:
interruptHandler0:
    IRET

.section .data
.skip 10
.word 0xABCD

@ Interrupt vector table
@ ===================================================
.section .iv_table
@ ===================================================
.word interruptHandler0
.rept 3
    .word defaultHandler
.endr

