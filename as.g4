grammar as;

translationUnit
    : optionList
    ;

optionList
    : optionsEach
    | optionList optionsEach
;

optionsEach
    :   directiveNotation           #optionDirectiveNotation
    |   instructionNotation         #optionInstructionNotation
    |   labelOnly ':'               #optionLabelOnly
    ;

labelOnly
    :   labelName
    ;

directiveNotation
    : label? directive
    ;

directive
    :   '.rept'    NumValue                    #d_rept
    |   '.endr'                                #d_endr
    |   directiveAllocation                    #d_allocation
    |   '.section'   SectionName               #d_section
    ;

directiveAllocation
    :   '.byte'  allocationDirectiveSuffix   #d_byte
    |   '.word'  allocationDirectiveSuffix   #d_word
    |   '.skip'  NumValue                    #d_skip
    |   '.int'   allocationDirectiveSuffix   #d_int
    |   '.asciz' StringLiteral               #d_asciz
    |   '.ascii' StringLiteral               #d_ascii
    ;

allocationDirectiveSuffix
    :   numberSingle
    |   allocationDirectiveSuffix ',' numberSingle
    ;

numberSingle
    :   number              #adsNumber
    |   labelName           #adsLabel
    ;

instructionNotation
    : label? instructionName addressing?
;

label
    : labelName ':'
    ;

labelName
    : LabelLiteral
    ;

SectionName
    :   [\\.][a-zA-Z_][a-zA-Z0-9_]*
    ;

instructionName
    : addresslessInstructions
    | addressInstructions
    | branchInstructions
    ;

addresslessInstructions
    : ( 'PUSH' | 'push' )       #I_PUSH
    | ( 'POP' | 'pop' )         #I_POP
    | ( 'RET' | 'ret' )         #I_RET
    | ( 'IRET'| 'iret' )        #I_IRET
    | ( 'HALT'| 'halt' )        #I_HALT
    | ( 'INC' | 'inc' )         #I_INC
    | ( 'DEC' | 'dec' )         #I_DEC
    | ( 'CLI' | 'cli' )         #I_CLI
    | ( 'STI' | 'sti' )         #I_STI
    | ( 'LDSP' | 'ldsp' )       #I_LDSP
    | ( 'STSP' | 'stsp' )       #I_STSP
    | ( 'LDIMR' | 'ldimr' )     #I_LDIMR
    | ( 'STIMR' | 'stimr' )     #I_STIMR
    ;

addressInstructions
    : ( 'LD' | 'ld' )     #IA_LD
    | ( 'ST' | 'st' )     #IA_ST
    | ( 'ADD' | 'add' )   #IA_ADD
    | ( 'AND' | 'and' )   #IA_AND
    | ( 'OR' | 'or' )     #IA_OR
    | ( 'NOT' | 'not' )   #IA_NOT
    | ( 'SUB' | 'sub' )   #IA_SUB
    | ( 'XOR' | 'xor' )   #IA_XOR
    | ( 'MUL' | 'mul' )   #IA_MUL
    | ( 'CMP' | 'cmp' )   #IA_CMP
    ;

branchInstructions
    : ( 'CALL' | 'call' )   #IB_CALL
    | ( 'BZ' | 'bz' )       #IB_BZ
    | ( 'JMP' | 'jmp' )     #IB_JMP
    | ( 'JGT' | 'bz' )      #IB_JGT
    | ( 'JGE' | 'bz' )      #IB_JGE
    | ( 'JLT' | 'bz' )      #IB_JLT
    | ( 'JLE' | 'bz' )      #IB_JLE
    | ( 'JEQ' | 'bz' )      #IB_JEQ
    | ( 'JNEQ' | 'bz' )     #IB_JNEQ
    ;

addressing
    :   registerIndirect
    |   memoryDirect
    |   memoryIndirect
    |   immediate
    |   memoryDirectLabel
    ;

memoryDirectLabel
    :  plusPlus?  labelName minusMinus?
    ;

plusPlus
    :   '++'
    ;

minusMinus
    :   '--'
    ;

// Addressing types
registerIndirect
    :   '[' LabelLiteral ']' registerIndirectDisp?
    ;

registerIndirectDisp
    :   ',' minus? number
    ;

minus
    :   '-'
    ;

memoryDirect
    :   HexValue
    ;

memoryIndirect
    :   '[' HexValue ']'
    ;

immediate
    : '#' number            #immediateNumber
    | '#' LabelLiteral      #immediateLabel
    ;

number
    :   HexValue     #numberHex
    |   NumValue     #numberDec
    |   BinValue     #numberBinary
    ;

LabelLiteral
    :   [a-zA-Z_][a-zA-Z0-9_]*
    ;

StringLiteral
    : '"' ~ [\r\n]* '"'
    ;

HexValue
    :  ('0x' | '0X') HEXADECIMALDIGIT+
    ;

BinValue
    :   '0' [bB] BINARYDIGIT+
    ;

NumValue
    : ([0]|[1-9][0-9]*)
    ;

fragment HEXADECIMALDIGIT
   : [0-9a-fA-F]
   ;

fragment BINARYDIGIT
      : ([01][01_]*[01]|[01])
      ;

Whitespace
   : [ \t]+ -> skip
   ;

Newline
   : ('\r' '\n'? | '\n') -> skip
   ;

BlockComment
   : '/*' .*? '*/' -> skip
   ;

LineComment
   : '@' ~ [\r\n]* -> skip
   ;